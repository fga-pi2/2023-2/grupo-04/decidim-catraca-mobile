// ignore_for_file: invalid_use_of_protected_member, invalid_use_of_visible_for_testing_member, use_build_context_synchronously

import 'package:decidim_catraca_mobile/components/gif_page.dart';
import 'package:decidim_catraca_mobile/models/event.dart';
import 'package:decidim_catraca_mobile/views/admin/edit_events/edit_events_controller.dart';
import 'package:decidim_catraca_mobile/views/admin/home/admin_home.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';

import '../../../../components/admin_header.dart';
import '../../../../components/event_preview.dart';
import '../../../../shared/shared.dart';

class EditEventsPage extends StatefulWidget {
  const EditEventsPage({super.key, required this.event});

  final EventModel event;

  @override
  State<EditEventsPage> createState() => _EditEventsPageState();
}

class _EditEventsPageState extends State<EditEventsPage> {
  EditEventsController controller = EditEventsController();

  @override
  void initState() {
    controller.initControllerValues(widget.event);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    const normalBorder = OutlineInputBorder(
      borderRadius: BorderRadius.all(
        Radius.circular(20),
      ),
      borderSide: BorderSide(
        color: AppColors.primaryRed,
        width: 2,
      ),
    );
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      body: Stack(
        children: [
          Column(
            children: [
              const AdminHeader(showBackButton: true),
              Expanded(
                child: Padding(
                  padding: EdgeInsets.symmetric(horizontal: size.width * 0.039),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          const Text(
                            "Editar Evento",
                            style: AppStyles.pageTitle24,
                          ),
                          IconButton(
                              onPressed: () {},
                              icon: const Icon(
                                Icons.restore_from_trash_rounded,
                                color: AppColors.primaryRed,
                              ))
                        ],
                      ),
                      ValueListenableBuilder(
                        valueListenable: controller.pageState,
                        builder: (context, value, child) => value ==
                                PageState.LOADED
                            ? Expanded(
                                child: SingleChildScrollView(
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      ValueListenableBuilder(
                                        valueListenable: controller.nameCounter,
                                        builder: (context, value, child) =>
                                            Container(
                                          margin: EdgeInsets.only(
                                              top: size.height * 0.026),
                                          child: TextFormField(
                                            decoration: InputDecoration(
                                              focusedBorder: normalBorder,
                                              enabledBorder: normalBorder,
                                              errorBorder: normalBorder,
                                              focusedErrorBorder: normalBorder,
                                              labelText: "Nome",
                                              labelStyle:
                                                  AppStyles.inputRedLabelStyle,
                                              constraints: BoxConstraints(
                                                  maxHeight:
                                                      size.height * 0.048),
                                              helperText: null,
                                              contentPadding: EdgeInsets.only(
                                                left: size.width * 0.05,
                                                right: size.width * 0.05,
                                              ),
                                              errorStyle:
                                                  AppStyles.red10TitleStyle,
                                              suffixText:
                                                  "${controller.nameCounter.value}/54",
                                              suffixStyle: const TextStyle(
                                                color: Color(0XFF989898),
                                                fontSize: 10,
                                                fontWeight: FontWeight.bold,
                                              ),
                                            ),
                                            cursorColor: Colors.black,
                                            cursorHeight: 15,
                                            controller:
                                                controller.nameFieldController,
                                            expands: false,
                                            style: AppStyles.inputLabelStyle,
                                            inputFormatters: [
                                              LengthLimitingTextInputFormatter(
                                                  54)
                                            ],
                                            onChanged: (value) =>
                                                controller.updateNameCounter(),
                                          ),
                                        ),
                                      ),
                                      Container(
                                        margin: EdgeInsets.only(
                                            top: size.height * 0.026),
                                        child: TextFormField(
                                          decoration: InputDecoration(
                                            focusedBorder: normalBorder,
                                            enabledBorder: normalBorder,
                                            errorBorder: normalBorder,
                                            focusedErrorBorder: normalBorder,
                                            labelText: "Data",
                                            labelStyle:
                                                AppStyles.inputRedLabelStyle,
                                            constraints: BoxConstraints(
                                                maxHeight: size.height * 0.048),
                                            helperText: null,
                                            contentPadding: EdgeInsets.only(
                                              left: size.width * 0.05,
                                              right: size.width * 0.05,
                                            ),
                                            errorStyle:
                                                AppStyles.red10TitleStyle,
                                            suffixIcon: const Icon(
                                              Icons.calendar_month_outlined,
                                              color: AppColors.primaryRed,
                                            ),
                                          ),
                                          cursorColor: Colors.black,
                                          cursorHeight: 15,
                                          controller:
                                              controller.dateFieldController,
                                          expands: false,
                                          style: AppStyles.inputLabelStyle,
                                          readOnly: true,
                                          onTap: () async {
                                            DateTime? pickedDate =
                                                await showDatePicker(
                                              context: context,
                                              initialDate: DateTime.now(),
                                              firstDate: DateTime.now(),
                                              //DateTime.now() - not to allow to choose before today.
                                              lastDate: DateTime(2050),
                                            );

                                            if (pickedDate != null) {
                                              String formattedDate =
                                                  DateFormat('yyyy/MM/dd')
                                                      .format(pickedDate);

                                              setState(() {
                                                controller.dateFieldController
                                                    .text = formattedDate;
                                              });
                                            }
                                          },
                                        ),
                                      ),
                                      Container(
                                        margin: EdgeInsets.only(
                                            top: size.height * 0.026),
                                        child: TextFormField(
                                          decoration: InputDecoration(
                                            focusedBorder: normalBorder,
                                            enabledBorder: normalBorder,
                                            errorBorder: normalBorder,
                                            focusedErrorBorder: normalBorder,
                                            labelText: "Localização",
                                            labelStyle:
                                                AppStyles.inputRedLabelStyle,
                                            constraints: BoxConstraints(
                                                maxHeight: size.height * 0.048),
                                            helperText: null,
                                            contentPadding: EdgeInsets.only(
                                              left: size.width * 0.05,
                                              right: size.width * 0.05,
                                            ),
                                            errorStyle:
                                                AppStyles.red10TitleStyle,
                                            suffixIcon: const Icon(
                                              Icons.location_on_sharp,
                                              color: AppColors.primaryRed,
                                            ),
                                          ),
                                          cursorColor: Colors.black,
                                          cursorHeight: 15,
                                          controller: controller
                                              .locationFieldController,
                                          expands: false,
                                          style: AppStyles.inputLabelStyle,
                                          onChanged: (value) => controller
                                              .cardNotifier
                                              .notifyListeners(),
                                        ),
                                      ),
                                      Container(
                                        margin: EdgeInsets.only(
                                            top: size.height * 0.026,
                                            bottom: size.height * 0.01),
                                        child: const Text(
                                          "Programação",
                                          style: AppStyles.pageTitle18,
                                        ),
                                      ),
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          TextFormField(
                                            decoration: InputDecoration(
                                              focusedBorder: normalBorder,
                                              enabledBorder: normalBorder,
                                              errorBorder: normalBorder,
                                              focusedErrorBorder: normalBorder,
                                              labelText: "Início",
                                              labelStyle:
                                                  AppStyles.inputRedLabelStyle,
                                              constraints: BoxConstraints(
                                                  maxHeight:
                                                      size.height * 0.048,
                                                  maxWidth: size.width * 0.44),
                                              helperText: null,
                                              contentPadding: EdgeInsets.only(
                                                left: size.width * 0.05,
                                                right: size.width * 0.05,
                                              ),
                                              errorStyle:
                                                  AppStyles.red10TitleStyle,
                                              suffixIcon: const Icon(
                                                Icons.timer_sharp,
                                                color: AppColors.primaryRed,
                                              ),
                                            ),
                                            cursorColor: Colors.black,
                                            cursorHeight: 15,
                                            controller: controller
                                                .startTimeFieldController,
                                            expands: false,
                                            style: AppStyles.inputLabelStyle,
                                            onTap: () async {
                                              TimeOfDay? pickedTime =
                                                  await showTimePicker(
                                                initialTime: TimeOfDay.now(),
                                                context: context,
                                                builder: (context, child) =>
                                                    MediaQuery(
                                                  data: MediaQuery.of(context)
                                                      .copyWith(
                                                          alwaysUse24HourFormat:
                                                              false),
                                                  child: child!,
                                                ),
                                              );

                                              if (pickedTime != null) {
                                                controller
                                                        .startTimeFieldController
                                                        .text =
                                                    "${pickedTime.hourOfPeriod}:${pickedTime.minute < 10 ? "0${pickedTime.minute}" : pickedTime.minute} ${pickedTime.period.name.toUpperCase()}";
                                              }
                                            },
                                          ),
                                          TextFormField(
                                            decoration: InputDecoration(
                                              focusedBorder: normalBorder,
                                              enabledBorder: normalBorder,
                                              errorBorder: normalBorder,
                                              focusedErrorBorder: normalBorder,
                                              labelText: "Fim",
                                              labelStyle:
                                                  AppStyles.inputRedLabelStyle,
                                              constraints: BoxConstraints(
                                                  maxHeight:
                                                      size.height * 0.048,
                                                  maxWidth: size.width * 0.44),
                                              helperText: null,
                                              contentPadding: EdgeInsets.only(
                                                left: size.width * 0.05,
                                                right: size.width * 0.05,
                                              ),
                                              errorStyle:
                                                  AppStyles.red10TitleStyle,
                                              suffixIcon: const Icon(
                                                Icons.timer_sharp,
                                                color: AppColors.primaryRed,
                                              ),
                                            ),
                                            cursorColor: Colors.black,
                                            cursorHeight: 15,
                                            controller: controller
                                                .endTimeFieldController,
                                            expands: false,
                                            style: AppStyles.inputLabelStyle,
                                            onTap: () async {
                                              TimeOfDay? pickedTime =
                                                  await showTimePicker(
                                                initialTime: TimeOfDay.now(),
                                                context: context,
                                                builder: (context, child) =>
                                                    MediaQuery(
                                                  data: MediaQuery.of(context)
                                                      .copyWith(
                                                          alwaysUse24HourFormat:
                                                              false),
                                                  child: child!,
                                                ),
                                              );

                                              if (pickedTime != null) {
                                                controller
                                                        .endTimeFieldController
                                                        .text =
                                                    "${pickedTime.hourOfPeriod}:${pickedTime.minute < 10 ? "0${pickedTime.minute}" : pickedTime.minute} ${pickedTime.period.name.toUpperCase()}";
                                              }
                                            },
                                          ),
                                        ],
                                      ),
                                      Container(
                                        margin: EdgeInsets.only(
                                            top: size.height * 0.026),
                                        child: TextFormField(
                                          decoration: InputDecoration(
                                            focusedBorder: normalBorder,
                                            enabledBorder: normalBorder,
                                            errorBorder: normalBorder,
                                            focusedErrorBorder: normalBorder,
                                            labelText: "Desrição",
                                            labelStyle:
                                                AppStyles.inputRedLabelStyle,
                                            constraints: BoxConstraints(
                                                maxHeight: size.height * 0.048),
                                            helperText: null,
                                            contentPadding: EdgeInsets.only(
                                              left: size.width * 0.05,
                                              right: size.width * 0.05,
                                            ),
                                            errorStyle:
                                                AppStyles.red10TitleStyle,
                                          ),
                                          cursorColor: Colors.black,
                                          cursorHeight: 15,
                                          controller: controller
                                              .descriptionFieldController,
                                          expands: false,
                                          style: AppStyles.inputLabelStyle,
                                        ),
                                      ),
                                      ValueListenableBuilder(
                                        valueListenable:
                                            controller.scheduleError,
                                        builder: (context, value, child) =>
                                            Visibility(
                                          visible: value,
                                          child: Container(
                                            margin: EdgeInsets.only(
                                              top: size.height * 0.02,
                                            ),
                                            child: Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.center,
                                              children: [
                                                SizedBox(
                                                  height: size.height * 0.02,
                                                  width: size.width * 0.05,
                                                  child: Image.asset(
                                                    "assets/images/warning.png",
                                                  ),
                                                ),
                                                Text(
                                                  controller
                                                      .scheduleErrorMessage,
                                                  style: AppStyles
                                                      .black13TitleStyle,
                                                ),
                                              ],
                                            ),
                                          ),
                                        ),
                                      ),
                                      Center(
                                        child: Container(
                                          height: size.height * 0.03,
                                          width: size.width * 0.5,
                                          margin: EdgeInsets.only(
                                            bottom: size.height * 0.015,
                                            top: size.height * 0.025,
                                          ),
                                          decoration: BoxDecoration(
                                            color: Colors.black,
                                            borderRadius:
                                                BorderRadius.circular(10),
                                          ),
                                          child: ElevatedButton(
                                            onPressed: () {
                                              controller.addScheduleToEvent();
                                            },
                                            style: AppStyles
                                                .elevatedRedButtonStyle,
                                            child: const Center(
                                              child: Text(
                                                "Adicionar Programação",
                                                style:
                                                    AppStyles.white13TitleStyle,
                                              ),
                                            ),
                                          ),
                                        ),
                                      ),
                                      ValueListenableBuilder(
                                        valueListenable:
                                            controller.eventScheduleNotifier,
                                        builder: (context, value, child) =>
                                            Column(
                                          children:
                                              controller.createScheduleTexts(),
                                        ),
                                      ),
                                      const Text(
                                        "Preview do Card",
                                        style: AppStyles.pageTitle24,
                                      ),
                                      Container(
                                        margin: EdgeInsets.only(
                                            top: size.height * 0.017),
                                        child: Row(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: [
                                            SizedBox(
                                              width: size.width * 0.1,
                                            ),
                                            ValueListenableBuilder(
                                              valueListenable:
                                                  controller.cardNotifier,
                                              builder:
                                                  (context, value, child) =>
                                                      Center(
                                                child: EventPreview(
                                                  event: controller
                                                      .returnEventModelObject(
                                                          1),
                                                ),
                                              ),
                                            ),
                                            SizedBox(
                                              width: size.width * 0.1,
                                              child: Column(
                                                children: [
                                                  ElevatedButton(
                                                    onPressed: () {
                                                      controller.cardColor =
                                                          AppColors.primaryRed;
                                                      controller.cardNotifier
                                                          .notifyListeners();
                                                    },
                                                    style: AppStyles
                                                        .elevatedNavBarButton,
                                                    child: Container(
                                                      width: size.width * 0.067,
                                                      height:
                                                          size.height * 0.031,
                                                      decoration: BoxDecoration(
                                                        color: const Color(
                                                            0XFFE53D57),
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(10),
                                                      ),
                                                    ),
                                                  ),
                                                  ElevatedButton(
                                                    onPressed: () {
                                                      controller.cardColor =
                                                          Colors.black;
                                                      controller.cardNotifier
                                                          .notifyListeners();
                                                    },
                                                    style: AppStyles
                                                        .elevatedNavBarButton,
                                                    child: Container(
                                                      width: size.width * 0.067,
                                                      height:
                                                          size.height * 0.031,
                                                      decoration: BoxDecoration(
                                                        color: Colors.black,
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(10),
                                                      ),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            )
                                          ],
                                        ),
                                      ),
                                      ValueListenableBuilder(
                                        valueListenable: controller.createError,
                                        builder: (context, value, child) =>
                                            Visibility(
                                          visible: value,
                                          child: Container(
                                            margin: EdgeInsets.only(
                                              top: size.height * 0.02,
                                            ),
                                            child: Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.center,
                                              children: [
                                                SizedBox(
                                                  height: size.height * 0.02,
                                                  width: size.width * 0.05,
                                                  child: Image.asset(
                                                    "assets/images/warning.png",
                                                  ),
                                                ),
                                                Text(
                                                  controller
                                                      .createEventErrorMessage,
                                                  style: AppStyles
                                                      .black13TitleStyle,
                                                ),
                                              ],
                                            ),
                                          ),
                                        ),
                                      ),
                                      Center(
                                        child: Container(
                                          height: size.height * 0.038,
                                          width: size.width * 0.5,
                                          margin: EdgeInsets.only(
                                            top: size.height * 0.02,
                                            bottom: size.height * 0.015,
                                          ),
                                          decoration: BoxDecoration(
                                            color: Colors.black,
                                            borderRadius:
                                                BorderRadius.circular(10),
                                          ),
                                          child: ElevatedButton(
                                            onPressed: () async {
                                              bool isValid =
                                                  controller.validateNewEvent();
                                              if (isValid) {
                                                await controller
                                                    .sendUpdateEventRequest(
                                                        widget.event.id);
                                                if (controller
                                                        .pageState.value ==
                                                    PageState.LOADED) {
                                                  controller.errorGif = false;
                                                  controller.showGif.value =
                                                      true;
                                                } else {
                                                  controller.errorGif = true;
                                                  controller.showGif.value =
                                                      true;
                                                }
                                              }
                                            },
                                            style: AppStyles
                                                .elevatedRedButtonStyle,
                                            child: const Center(
                                              child: Text(
                                                "Atualizar",
                                                style:
                                                    AppStyles.white14TitleStyle,
                                              ),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              )
                            : value == PageState.LOADING
                                ? const Center(
                                    child: CircularProgressIndicator(),
                                  )
                                : const Center(
                                    child: Text("Erro"),
                                  ),
                      ),
                    ],
                  ),
                ),
              )
            ],
          ),
          ValueListenableBuilder(
            valueListenable: controller.pageState,
            builder: (context, value, child) => value == PageState.LOADING
                ? Container(
                    height: size.height,
                    width: size.width,
                    color: Colors.black.withOpacity(.3),
                    child: const Center(
                      child: CircularProgressIndicator(
                        color: AppColors.primaryRed,
                      ),
                    ),
                  )
                : const SizedBox(),
          ),
          ValueListenableBuilder(
            valueListenable: controller.showGif,
            builder: (context, value, child) => value
                ? GifState(
                    error: controller.errorGif,
                    function: controller.errorGif
                        ? () {
                            controller.showGif.value = false;
                          }
                        : () {
                            controller.showGif.value = false;
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => const AdminHomePage(),
                              ),
                            );
                          })
                : const SizedBox(),
          )
        ],
      ),
      backgroundColor: Colors.white,
    );
  }
}
