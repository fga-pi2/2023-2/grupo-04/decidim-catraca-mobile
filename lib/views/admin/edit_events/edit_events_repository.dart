import 'dart:developer';
import 'dart:io';

import 'package:decidim_catraca_mobile/interfaces/response_interface.dart';
import 'package:decidim_catraca_mobile/models/event.dart';
import 'package:decidim_catraca_mobile/models/event_schedule.dart';
import 'package:dio/dio.dart';
import 'package:intl/intl.dart';

import '../../../../shared/app_constants.dart';

class EditEventRepository {
  final dio = Dio(BaseOptions(
    baseUrl: AppConstants.BASE_URL,
    connectTimeout: const Duration(seconds: 15),
  ));

  Future<ResponseInterface> editEvent(EventModel event) async {
    Response response;

    DateTime dateTime = DateFormat("yyyy/MM/dd").parse(event.date);
    String formattedDateTime = dateTime.toUtc().toIso8601String();
    List<Object> scheduleBody = [];
    for (EventSchedule schedule in event.eventSchedule) {
      scheduleBody.add({
        "scheduleEventId": schedule.id,
        "startTime": schedule.startTime,
        "endTime": schedule.endTime,
        "description": schedule.description
      });
    }
    try {
      response = await dio.put(
        "/events",
        options: Options(
          headers: {
            HttpHeaders.contentTypeHeader: "application/json",
            HttpHeaders.authorizationHeader: AppConstants.actualUser.token,
          },
        ),
        data: {
          "id": event.id,
          "name": event.name,
          "date": formattedDateTime,
          "local": event.location,
          "color":
              "#${event.color.value.toRadixString(16).substring(2).toUpperCase()}",
          "schedule": scheduleBody,
        },
      );
      log("Create Event OK");
      return ResponseInterface(response.data, 200);
    } on DioException catch (error) {
      if (error.response == null) {
        log("Problem with server");
        return ResponseInterface(null, 500);
      }
    }
    log("Problem with server");
    return ResponseInterface(null, 500);
  }

  Future<ResponseInterface> deleteEvent(int eventId) async {
    Response response;
    try {
      response = await dio.delete(
        "/events/$eventId",
        options: Options(
          headers: {
            HttpHeaders.contentTypeHeader: "application/json",
            HttpHeaders.authorizationHeader: AppConstants.actualUser.token,
          },
        ),
      );
      log("Deleted Event $eventId");
      return ResponseInterface(response.data, 200);
    } on DioException catch (error) {
      if (error.response == null) {
        log("Problem with server");
        return ResponseInterface(null, 500);
      }
    }
    log("Problem with server");
    return ResponseInterface(null, 500);
  }
}
