// ignore_for_file: constant_identifier_names, invalid_use_of_protected_member, invalid_use_of_visible_for_testing_member

import 'dart:developer';

import 'package:decidim_catraca_mobile/interfaces/response_interface.dart';
import 'package:decidim_catraca_mobile/models/event.dart';
import 'package:decidim_catraca_mobile/models/event_schedule.dart';
import 'package:decidim_catraca_mobile/views/admin/edit_events/edit_events_repository.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import '../../../../shared/shared.dart';

enum PageState { LOADING, LOADED, ERROR }

class EditEventsController {
  // Fields Controllers
  final TextEditingController nameFieldController = TextEditingController();
  final TextEditingController dateFieldController = TextEditingController();
  final TextEditingController startTimeFieldController =
      TextEditingController();
  final TextEditingController endTimeFieldController = TextEditingController();
  final TextEditingController descriptionFieldController =
      TextEditingController();
  final TextEditingController locationFieldController = TextEditingController();

  // Card Controller
  Color cardColor = AppColors.primaryRed;
  List<EventSchedule> eventSchedule = [];

  // Page controllers
  final ValueNotifier<int> nameCounter = ValueNotifier(0);
  final ValueNotifier cardNotifier = ValueNotifier(0);
  final ValueNotifier eventScheduleNotifier = ValueNotifier(0);
  final ValueNotifier scheduleError = ValueNotifier(false);
  final ValueNotifier createError = ValueNotifier(false);
  final ValueNotifier<PageState> pageState = ValueNotifier(PageState.LOADED);
  final ValueNotifier showGif = ValueNotifier(false);
  bool errorGif = false;

  // Repository
  EditEventRepository repository = EditEventRepository();

  // To identify differents schedules
  int eventId = 1;

  String scheduleErrorMessage = "";
  String createEventErrorMessage = "";

  sendUpdateEventRequest(int eventId) async {
    ResponseInterface response =
        await repository.editEvent(returnEventModelObject(eventId));

    if (response.statusCode == 200) {
      log("Event Updated");
      pageState.value = PageState.LOADED;
    } else {
      pageState.value = PageState.ERROR;
    }
  }

  sendDeleteEventRequest(int eventId) async {
    pageState.value = PageState.LOADING;
    ResponseInterface response = await repository.deleteEvent(eventId);
    pageState.value = PageState.LOADED;

    if (response.statusCode == 200) {
      errorGif = false;
      showGif.value = true;
    } else {
      errorGif = true;
      showGif.value = true;
    }
  }

  void initControllerValues(EventModel event) {
    nameFieldController.text = event.name;
    nameCounter.value = nameFieldController.text.length;
    dateFieldController.text = event.date;
    locationFieldController.text = event.location;
    eventSchedule = event.eventSchedule;
    cardColor = event.color;
  }

  void updateNameCounter() {
    nameCounter.value = nameFieldController.text.length;
    cardNotifier.notifyListeners();
  }

  void addScheduleToEvent() {
    scheduleError.value = false;
    if (validateNewSchedule()) {
      EventSchedule newEvent = EventSchedule(
          eventId,
          startTimeFieldController.text,
          endTimeFieldController.text,
          descriptionFieldController.text);
      eventId++;

      eventSchedule.add(newEvent);

      startTimeFieldController.clear();
      endTimeFieldController.clear();
      descriptionFieldController.clear();
      cardNotifier.notifyListeners();
      eventScheduleNotifier.notifyListeners();
    } else {
      if (startTimeFieldController.text.isEmpty) {
        scheduleErrorMessage = "Start Time is required";
      } else if (endTimeFieldController.text.isEmpty) {
        scheduleErrorMessage = "End Time is required";
      } else if (descriptionFieldController.text.isEmpty) {
        scheduleErrorMessage = "Description is required";
      } else {
        scheduleErrorMessage = "Start Time set after End Time";
      }
      scheduleError.value = true;
    }
  }

  bool validateNewSchedule() {
    if (startTimeFieldController.text == "" ||
        endTimeFieldController.text == "" ||
        descriptionFieldController.text == "") return false;

    DateTime startTime =
        DateFormat("HH:mm a").parse(startTimeFieldController.text);
    DateTime endTime = DateFormat("HH:mm a").parse(endTimeFieldController.text);

    if (startTime.isAfter(endTime)) return false;

    return true;
  }

  void removeEventScheduleFromList(int id) {
    eventSchedule.removeWhere((element) => element.id == id);
  }

  List<Widget> createScheduleTexts() {
    List<Widget> result = [];
    for (var element in eventSchedule) {
      result.add(Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            "${element.startTime}-${element.endTime} - ${element.description}",
            style: AppStyles.black12semiBold,
          ),
          IconButton(
            onPressed: () {
              removeEventScheduleFromList(element.id);
              eventScheduleNotifier.notifyListeners();
            },
            icon: const Icon(
              Icons.highlight_remove_sharp,
              color: AppColors.primaryRed,
            ),
          )
        ],
      ));
    }

    return result;
  }

  bool validateNewEvent() {
    createError.value = false;
    if (nameFieldController.text.isEmpty) {
      createEventErrorMessage = "Name is required for an event!";
      createError.value = true;
      return false;
    } else if (dateFieldController.text.isEmpty) {
      createEventErrorMessage = "Date is required for an event!";
      createError.value = true;
      return false;
    } else if (locationFieldController.text.isEmpty) {
      createEventErrorMessage = "Location is required for an event!";
      createError.value = true;
      return false;
    } else if (eventSchedule.isEmpty) {
      createEventErrorMessage = "Add at least 1 schedule to the event!";
      createError.value = true;
      return false;
    } else {
      // Call create event
      pageState.value = PageState.LOADING;
      return true;
    }
  }

  EventModel returnEventModelObject(int eventId) {
    return EventModel(
      eventId,
      nameFieldController.text,
      dateFieldController.text,
      locationFieldController.text,
      cardColor,
      eventSchedule,
    );
  }
}
