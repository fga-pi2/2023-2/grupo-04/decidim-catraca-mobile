import 'dart:developer';

import 'package:decidim_catraca_mobile/interfaces/response_interface.dart';
import 'package:decidim_catraca_mobile/models/admin/event_participant.dart';
import 'package:decidim_catraca_mobile/views/admin/events_list/events_list_repository.dart';
import 'package:flutter/material.dart';

// ignore: constant_identifier_names
enum EvetsListState { LOADING, LOADED, ERROR }

class EventsListController {
  ValueNotifier<EvetsListState> pageState =
      ValueNotifier(EvetsListState.LOADING);
  ValueNotifier<String> searchText = ValueNotifier("");

  List<EventParticipant> participants = [];
  List<EventParticipant> filteredParticipants = [];
  TextEditingController searchController = TextEditingController();

  EventsListRepository repository = EventsListRepository();

  getEventParticipants(int eventId) async {
    ResponseInterface response = await repository.sendNewEvent(eventId);

    if (response.statusCode == 200) {
      log("Get Event Participants");

      List<dynamic> users = response.data["users"];

      for (var element in users) {
        participants.add(EventParticipant.fromMap(element));
      }

      filteredParticipants = participants;

      pageState.value = EvetsListState.LOADED;
    } else {
      pageState.value = EvetsListState.ERROR;
    }
  }

  sendDeleteUserRequest(String participantId, String eventId) async {
    pageState.value = EvetsListState.LOADING;
    ResponseInterface response =
        await repository.deleteUserInEvent(participantId, eventId);

    if (response.statusCode == 200) {
      log("User deleted!");

      searchText.value = "";
      participants.removeWhere((element) => element.id == participantId);
      filteredParticipants = participants;

      pageState.value = EvetsListState.LOADED;
      return 200;
    } else {
      pageState.value = EvetsListState.LOADED;
      return 500;
    }
  }

  filteredAmbassadorsList() {
    filteredParticipants = participants;

    if (searchText.value != "") {
      filteredParticipants = filteredParticipants
          .where((element) =>
              element.fullName
                  .toLowerCase()
                  .contains(searchText.value.toLowerCase()) ||
              element.id.contains(searchText.value))
          .toList();
    }
  }

  updateSearchText() {
    searchText.value = searchController.text;
    filteredAmbassadorsList();
  }
}
