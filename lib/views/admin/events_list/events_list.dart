import 'package:decidim_catraca_mobile/shared/app_styles.dart';
import 'package:decidim_catraca_mobile/views/admin/events/components/event_participant_card.dart';
import 'package:decidim_catraca_mobile/views/admin/events_list/events_list_controller.dart';
import 'package:flutter/material.dart';

import '../../../../components/admin_header.dart';
import '../../../models/event.dart';

class EventsListPage extends StatefulWidget {
  const EventsListPage({super.key, required this.event});

  final EventModel event;

  @override
  State<EventsListPage> createState() => _EventsListPageState();
}

class _EventsListPageState extends State<EventsListPage> {
  final EventsListController controller = EventsListController();

  final border = OutlineInputBorder(
    borderSide: const BorderSide(
      color: Colors.black,
    ),
    borderRadius: BorderRadius.circular(40),
  );

  @override
  void initState() {
    controller.getEventParticipants(widget.event.id);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      body: SingleChildScrollView(
        child: ValueListenableBuilder(
          valueListenable: controller.pageState,
          builder: (context, value, child) => value == EvetsListState.LOADED
              ? Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const AdminHeader(
                      showSignOutButton: true,
                      showBackButton: true,
                    ),
                    Padding(
                      padding:
                          EdgeInsets.symmetric(horizontal: size.width * 0.039),
                      child: Text(
                        "Participantes - ${widget.event.name}",
                        style: AppStyles.pageTitle24,
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: size.height * 0.011),
                      padding:
                          EdgeInsets.symmetric(horizontal: size.width * 0.039),
                      child: TextField(
                        decoration: InputDecoration(
                          fillColor: Colors.black,
                          filled: true,
                          suffixIcon: const Icon(
                            Icons.search,
                            color: Colors.white,
                            size: 25,
                          ),
                          constraints:
                              BoxConstraints(maxHeight: size.height * 0.048),
                          contentPadding: EdgeInsets.only(
                            left: size.width * 0.05 + 25,
                            right: size.width * 0.02,
                          ),
                          border: border,
                          focusedBorder: border,
                          enabledBorder: border,
                          hintText: "Procure por nome ou id do usuário",
                          hintStyle: AppStyles.white13TitleStyle,
                        ),
                        cursorColor: Colors.white,
                        style: AppStyles.white13TitleStyle,
                        textAlign: TextAlign.center,
                        controller: controller.searchController,
                        onChanged: (value) {
                          controller.updateSearchText();
                        },
                      ),
                    ),
                    Scrollbar(
                      child: Container(
                        height: size.height * 0.55,
                        margin: EdgeInsets.only(top: size.height * 0.037),
                        padding: EdgeInsets.symmetric(
                            horizontal: size.width * 0.039),
                        child: ValueListenableBuilder(
                          valueListenable: controller.searchText,
                          builder: (context, value, child) => ListView.builder(
                            padding: EdgeInsets.zero,
                            itemCount: controller.filteredParticipants.length,
                            itemBuilder: (context, index) =>
                                EventParticipantCard(
                              participant:
                                  controller.filteredParticipants[index],
                              controller: controller,
                              event: widget.event,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                )
              : const SizedBox(),
        ),
      ),
    );
  }
}
