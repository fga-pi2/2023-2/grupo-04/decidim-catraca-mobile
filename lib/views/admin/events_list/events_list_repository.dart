import 'dart:developer';
import 'dart:io';

import 'package:decidim_catraca_mobile/interfaces/response_interface.dart';
import 'package:decidim_catraca_mobile/shared/app_constants.dart';
import 'package:dio/dio.dart';

class EventsListRepository {
  final dio = Dio(BaseOptions(
    baseUrl: AppConstants.BASE_URL,
    connectTimeout: const Duration(seconds: 15),
  ));

  Future<ResponseInterface> sendNewEvent(int id) async {
    Response response;
    try {
      response = await dio.get(
        "/users/events/$id",
        options: Options(
          headers: {
            HttpHeaders.contentTypeHeader: "application/json",
            HttpHeaders.authorizationHeader: AppConstants.actualUser.token,
          },
        ),
      );
      log("Get Participants OK");
      return ResponseInterface(response.data, 200);
    } on DioException catch (error) {
      if (error.response == null) {
        log("Problem with server");
        return ResponseInterface(null, 500);
      }
    }
    log("Problem with server");
    return ResponseInterface(null, 500);
  }

  Future<ResponseInterface> deleteUserInEvent(
      String userId, String eventId) async {
    Response response;
    try {
      response = await dio.delete(
        "/users/events",
        data: {
          "userId": userId,
          "eventId": eventId,
        },
        options: Options(
          headers: {
            HttpHeaders.contentTypeHeader: "application/json",
            HttpHeaders.authorizationHeader: AppConstants.actualUser.token,
          },
        ),
      );

      log("Delete User");
      return ResponseInterface(response.data, 200);
    } on DioException catch (error) {
      if (error.response == null) {
        log("Problem with server");
        return ResponseInterface(null, 500);
      }
    }

    log("Problem with server");
    return ResponseInterface(null, 500);
  }
}
