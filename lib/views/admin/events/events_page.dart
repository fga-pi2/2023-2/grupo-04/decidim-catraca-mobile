// ignore_for_file: prefer_const_constructors, prefer_const_literals_to_create_immutables

import 'package:decidim_catraca_mobile/components/admin_header.dart';
import 'package:decidim_catraca_mobile/components/day_events_card.dart';
import 'package:decidim_catraca_mobile/views/admin/create_events/create_events.dart';
import 'package:decidim_catraca_mobile/views/admin/events/events_page_controller.dart';
import 'package:decidim_catraca_mobile/views/admin/home/controller/admin_home_controller.dart';
import 'package:flutter/material.dart';

import '../../../../shared/shared.dart';

class EventsAdminPage extends StatefulWidget {
  const EventsAdminPage({super.key, required this.homeController});

  final AdminHomeController homeController;

  @override
  State<EventsAdminPage> createState() => _EventsAdminPageState();
}

class _EventsAdminPageState extends State<EventsAdminPage> {
  final EventsPageController controller = EventsPageController();

  @override
  void initState() {
    controller.getEvents();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Column(
      children: [
        const AdminHeader(),
        ValueListenableBuilder(
          valueListenable: controller.pageState,
          builder: (context, value, child) => value == PageEventState.LOADED
              ? Column(
                  children: [
                    Padding(
                      padding:
                          EdgeInsets.symmetric(horizontal: size.width * 0.039),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          const Text(
                            "Eventos",
                            style: AppStyles.pageTitle24,
                          ),
                          Container(
                            height: size.height * 0.033,
                            width: size.width * 0.2,
                            decoration: BoxDecoration(
                              color: Colors.black,
                              borderRadius: BorderRadius.circular(10),
                            ),
                            child: ElevatedButton(
                              onPressed: () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                      builder: (context) => CreateEventsPage(),
                                    ));
                              },
                              style: AppStyles.elevatedRedButtonStyle,
                              child: Center(
                                child: Text(
                                  "Criar",
                                  style: AppStyles.white13TitleStyle,
                                ),
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                    Container(
                      height: size.height * 0.6,
                      margin: EdgeInsets.only(top: size.height * 0.02),
                      child: controller.listEvents.isNotEmpty
                          ? ListView.builder(
                              padding:
                                  EdgeInsets.only(bottom: size.height * 0.1),
                              itemCount: controller.listEvents.length,
                              itemBuilder: (context, index) => DayEventsCard(
                                weekDay: controller.listEvents[index].weekDay,
                                day: controller.listEvents[index].day,
                                month: controller.listEvents[index].month,
                                eventsList: controller.listEvents[index].events,
                                isEditEvent: true,
                              ),
                            )
                          : Align(
                              alignment: Alignment.topCenter,
                              child: Column(
                                children: [
                                  Container(
                                    height: size.height * 0.25,
                                    width: size.width * 0.6,
                                    margin: EdgeInsets.only(
                                      top: size.height * 0.08,
                                    ),
                                    child: Image.asset(
                                      "assets/images/empty-events.png",
                                    ),
                                  ),
                                  Text(
                                    "Não tem nenhum evento programado!",
                                    style: AppStyles.black12semiBold,
                                  )
                                ],
                              ),
                            ),
                    )
                  ],
                )
              : value == PageEventState.ERROR
                  ? SizedBox(
                      height: size.height * 0.5,
                      width: size.width,
                      child: Center(
                        child: Text(
                          "Sorry! We're having problems with server",
                          style: AppStyles.black20TitleStyle,
                          textAlign: TextAlign.center,
                        ),
                      ),
                    )
                  : SizedBox(),
        ),
      ],
    );
    // ValueListenableBuilder(
    //   valueListenable: controller.pageState,
    //   builder: (context, value, child) => value == PageState.LOADING
    //       ? Container(
    //           height: size.height,
    //           width: size.width,
    //           color: Colors.black.withOpacity(.2),
    //           child: Center(
    //             child: CircularProgressIndicator(),
    //           ),
    //         )
    //       : SizedBox(),
  }
}
