import 'dart:developer';
import 'dart:io';

import 'package:decidim_catraca_mobile/interfaces/response_interface.dart';
import 'package:decidim_catraca_mobile/shared/app_constants.dart';
import 'package:dio/dio.dart';

class EventsRepository {
  final dio = Dio(BaseOptions(
    baseUrl: AppConstants.BASE_URL,
    connectTimeout: const Duration(seconds: 15),
  ));

  Future<ResponseInterface> getEventsRequest() async {
    Response response;
    try {
      response = await dio.get(
        "/events",
        options: Options(
          headers: {
            HttpHeaders.contentTypeHeader: "application/json",
            HttpHeaders.authorizationHeader: AppConstants.actualUser.token,
          },
        ),
      );
      log("GET Events OK");
      return ResponseInterface(response.data["data"], 200);
    } on DioException catch (error) {
      if (error.response == null) {
        log("Problem with server");
        return ResponseInterface(null, 500);
      }
    }
    log("Problem with server");
    return ResponseInterface(null, 500);
  }

  Future<ResponseInterface> getLastEventsRequest() async {
    Response response;
    try {
      response = await dio.get(
        "/last-events",
        options: Options(
          headers: {
            HttpHeaders.contentTypeHeader: "application/json",
            HttpHeaders.authorizationHeader: AppConstants.actualUser.token,
          },
        ),
      );
      log("GET Events OK");
      return ResponseInterface(response.data["data"], 200);
    } on DioException catch (error) {
      if (error.response == null) {
        log("Problem with server");
        return ResponseInterface(null, 500);
      }
    }
    log("Problem with server");
    return ResponseInterface(null, 500);
  }

  Future<ResponseInterface> sendAmbassadorPresence(
      String ambassadorId, int eventId) async {
    Response response;
    try {
      response = await dio.post(
        "/presence",
        options: Options(
          headers: {
            HttpHeaders.contentTypeHeader: "application/json",
            HttpHeaders.authorizationHeader: AppConstants.actualUser.token,
          },
        ),
        data: {
          "userId": ambassadorId,
          "eventId": eventId,
        },
      );
      log("POST User: $ambassadorId to Event: $eventId");
      return ResponseInterface(response.data["data"], 200);
    } on DioException catch (error) {
      log(error.toString());
      if (error.response == null) {
        log("Problem with server");
        return ResponseInterface(null, 500);
      }
    }
    log("Problem with server");
    return ResponseInterface(null, 500);
  }
}
