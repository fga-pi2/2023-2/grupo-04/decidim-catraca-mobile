// ignore_for_file: sort_child_properties_last, use_build_context_synchronously

import 'dart:developer';

import 'package:decidim_catraca_mobile/models/event.dart';
import 'package:decidim_catraca_mobile/shared/app_colors.dart';
import 'package:decidim_catraca_mobile/shared/app_styles.dart';
import 'package:decidim_catraca_mobile/views/admin/edit_events/edit_events.dart';
import 'package:decidim_catraca_mobile/views/admin/events_list/events_list.dart';
import 'package:decidim_catraca_mobile/views/admin/events_list/events_list_controller.dart';
import 'package:flutter/material.dart';
import 'package:localstorage/localstorage.dart';
import 'package:toastification/toastification.dart';

class EventSheetOptions extends StatelessWidget {
  EventSheetOptions({super.key, required this.event});

  final EventsListController controller = EventsListController();

  final EventModel event;

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    LocalStorage storage = LocalStorage("eventParticipants.json");
    return Container(
      height: size.height,
      width: size.width,
      decoration: const BoxDecoration(
        borderRadius: BorderRadius.vertical(top: Radius.circular(35)),
        color: Colors.white,
      ),
      child: Column(
        children: [
          Container(
            margin: EdgeInsets.only(top: size.height * 0.05),
            child: Column(
              children: [
                Text(
                  event.name,
                  style: const TextStyle(
                    color: AppColors.primaryRed,
                    fontSize: 25,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                Text(
                  event.date,
                  style: const TextStyle(
                    color: AppColors.primaryRed,
                    fontSize: 16,
                  ),
                ),
              ],
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: size.height * 0.05),
            child: ElevatedButton(
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => EditEventsPage(event: event),
                  ),
                );
              },
              style: AppStyles.redButton,
              child: SizedBox(
                height: size.height * 0.05,
                width: size.width * 0.85,
                child: const Center(
                  child: Text(
                    "Editar Evento",
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 16,
                    ),
                  ),
                ),
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: size.height * 0.02),
            child: ElevatedButton(
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => EventsListPage(event: event),
                  ),
                );
              },
              style: AppStyles.redButton,
              child: SizedBox(
                height: size.height * 0.05,
                width: size.width * 0.85,
                child: const Center(
                  child: Text(
                    "Editar Participantes",
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 16,
                    ),
                  ),
                ),
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: size.height * 0.02),
            child: ElevatedButton(
              onPressed: () async {
                await controller.getEventParticipants(event.id);
                await storage.clear();
                storage.setItem(
                  "eventParticipants",
                  controller.filteredParticipants
                      .map((participant) => participant.toJSONEncodable())
                      .toList(),
                );
                toastification.show(
                  context: context,
                  type: ToastificationType.success,
                  style: ToastificationStyle.flat,
                  autoCloseDuration: const Duration(seconds: 5),
                  title: 'Encerramento de lista!',
                  description: 'Lista baixada para o dispositivo com sucesso',
                  alignment: Alignment.topRight,
                  direction: TextDirection.ltr,
                  animationDuration: const Duration(milliseconds: 300),
                  icon: const Icon(Icons.check),
                  primaryColor: Colors.green,
                  backgroundColor: Colors.green,
                  foregroundColor: Colors.white,
                  padding:
                      const EdgeInsets.symmetric(horizontal: 12, vertical: 16),
                  margin:
                      const EdgeInsets.symmetric(horizontal: 12, vertical: 8),
                  borderRadius: BorderRadius.circular(12),
                  boxShadow: const [
                    BoxShadow(
                      color: Color(0x07000000),
                      blurRadius: 16,
                      offset: Offset(0, 16),
                      spreadRadius: 0,
                    )
                  ],
                  showProgressBar: true,
                  closeButtonShowType: CloseButtonShowType.onHover,
                  closeOnClick: false,
                  pauseOnHover: true,
                  dragToClose: true,
                );
                log("Lista Encerrada");
              },
              style: AppStyles.redButton,
              child: SizedBox(
                height: size.height * 0.05,
                width: size.width * 0.85,
                child: const Center(
                  child: Text(
                    "Encerrar Lista",
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 16,
                    ),
                  ),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}
