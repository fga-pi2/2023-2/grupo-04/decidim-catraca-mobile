import 'package:decidim_catraca_mobile/components/server_error_dialog.dart';
import 'package:decidim_catraca_mobile/models/admin/event_participant.dart';
import 'package:decidim_catraca_mobile/models/event.dart';
import 'package:decidim_catraca_mobile/shared/app_colors.dart';
import 'package:decidim_catraca_mobile/shared/app_styles.dart';
import 'package:decidim_catraca_mobile/views/admin/events_list/events_list_controller.dart';
import 'package:flutter/material.dart';

class EventParticipantCard extends StatelessWidget {
  const EventParticipantCard(
      {super.key,
      required this.participant,
      required this.controller,
      required this.event});

  final EventParticipant participant;
  final EventsListController controller;
  final EventModel event;

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      width: size.width * 0.913,
      margin: EdgeInsets.only(bottom: size.height * 0.027),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                participant.fullName,
                style: AppStyles.black20TitleStyle,
              ),
              Text(
                "#${participant.id}",
                style: AppStyles.black16Regular,
              )
              // Text(controller.searchText.value)
            ],
          ),
          Container(
            height: size.height * 0.043,
            width: size.width * 0.255,
            decoration: BoxDecoration(
              color: AppColors.primaryRed,
              borderRadius: BorderRadius.circular(40),
            ),
            child: ElevatedButton(
              onPressed: () async {
                int statusCode = await controller.sendDeleteUserRequest(
                  participant.id,
                  event.id.toString(),
                );

                if (statusCode == 500) {
                  showDialog(
                    context: context,
                    builder: (context) => const ServerErrorDialog(),
                  );
                }
              },
              style: AppStyles.elevatedRedButtonStyle,
              child: const Center(
                child: Text(
                  "Remover",
                  style: AppStyles.white13TitleStyle,
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}
