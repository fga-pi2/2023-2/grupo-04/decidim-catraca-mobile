// ignore_for_file: constant_identifier_names

import 'package:decidim_catraca_mobile/interfaces/response_interface.dart';
import 'package:decidim_catraca_mobile/models/admin/events_list.dart';
import 'package:decidim_catraca_mobile/views/admin/events/events_page_repository.dart';
import 'package:flutter/material.dart';

enum PageEventState { LOADING, LOADED, ERROR }

class EventsPageController {
  EventsRepository repository = EventsRepository();
  ValueNotifier<PageEventState> pageState =
      ValueNotifier(PageEventState.LOADING);
  List<EventsList> listEvents = [];

  getEvents() async {
    ResponseInterface response = await repository.getEventsRequest();

    if (response.data == null) {
      pageState.value = PageEventState.ERROR;
    } else {
      List<dynamic> events = response.data;
      for (Map<dynamic, dynamic> dayOfEvent in events) {
        listEvents.add(EventsList.fromMap(dayOfEvent));
      }
      listEvents = listEvents.reversed.toList();
      pageState.value = PageEventState.LOADED;
    }
  }

  getLastEvents() async {
    ResponseInterface response = await repository.getLastEventsRequest();

    if (response.data == null) {
      pageState.value = PageEventState.ERROR;
    } else {
      List<dynamic> events = response.data;
      for (Map<dynamic, dynamic> dayOfEvent in events) {
        listEvents.add(EventsList.fromMap(dayOfEvent));
      }
      listEvents = listEvents.reversed.toList();
      pageState.value = PageEventState.LOADED;
    }
  }
}
