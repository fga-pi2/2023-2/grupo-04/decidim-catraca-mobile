import 'package:flutter/material.dart';

import '../../../../../shared/shared.dart';

class EventsTextField extends StatefulWidget {
  const EventsTextField({
    super.key,
    required this.labelText,
    required this.controller,
    this.obscureText = false,
  });

  final String labelText;
  final bool obscureText;
  final TextEditingController controller;

  @override
  State<EventsTextField> createState() => _EventsTextFieldState();
}

class _EventsTextFieldState extends State<EventsTextField> {
  final normalBorder = const OutlineInputBorder(
    borderRadius: BorderRadius.all(
      Radius.circular(20),
    ),
    borderSide: BorderSide(
      color: AppColors.primaryRed,
      width: 2,
    ),
  );

  final errorBorder = const OutlineInputBorder(
    borderRadius: BorderRadius.all(
      Radius.circular(20),
    ),
    borderSide: BorderSide(
      color: Colors.red,
      width: 2,
    ),
  );
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return TextFormField(
      decoration: InputDecoration(
        focusedBorder: normalBorder,
        enabledBorder: normalBorder,
        errorBorder: errorBorder,
        focusedErrorBorder: errorBorder,
        labelText: widget.labelText,
        labelStyle: AppStyles.inputRedLabelStyle,
        constraints: BoxConstraints(maxHeight: size.height * 0.048),
        helperText: null,
        contentPadding:
            EdgeInsets.only(left: size.width * 0.05, right: size.width * 0.02),
        errorStyle: AppStyles.red10TitleStyle,
      ),
      cursorColor: Colors.black,
      cursorHeight: 15,
      obscureText: widget.obscureText,
      controller: widget.controller,
      expands: false,
    );
  }
}
