import 'dart:developer';
import 'dart:io';

import 'package:decidim_catraca_mobile/interfaces/response_interface.dart';
import 'package:decidim_catraca_mobile/models/event.dart';
import 'package:decidim_catraca_mobile/models/event_schedule.dart';
import 'package:decidim_catraca_mobile/shared/app_constants.dart';
import 'package:dio/dio.dart';
import 'package:intl/intl.dart';

class CreateEventsRepository {
  final dio = Dio(BaseOptions(
    baseUrl: AppConstants.BASE_URL,
    connectTimeout: const Duration(seconds: 15),
  ));

  Future<ResponseInterface> sendNewEvent(EventModel event) async {
    Response response;

    DateTime dateTime = DateFormat("yyyy/MM/dd").parse(event.date);
    String formattedDateTime = dateTime.toUtc().toIso8601String();
    List<Object> scheduleBody = [];
    for (EventSchedule schedule in event.eventSchedule) {
      scheduleBody.add({
        "scheduleEventId": schedule.id,
        "startTime": schedule.startTime,
        "endTime": schedule.endTime,
        "description": schedule.description
      });
    }
    try {
      response = await dio.post(
        "/events",
        options: Options(
          headers: {
            HttpHeaders.contentTypeHeader: "application/json",
            HttpHeaders.authorizationHeader: AppConstants.actualUser.token,
          },
        ),
        data: {
          "name": event.name,
          "date": formattedDateTime,
          "local": event.location,
          "color":
              "#${event.color.value.toRadixString(16).substring(2).toUpperCase()}",
          "schedule": scheduleBody,
        },
      );
      log("Create Event OK");
      return ResponseInterface(response.data, 200);
    } on DioException catch (error) {
      if (error.response == null) {
        log("Problem with server");
        return ResponseInterface(null, 500);
      }
    }
    log("Problem with server");
    return ResponseInterface(null, 500);
  }
}
