// ignore_for_file: constant_identifier_names

import 'dart:convert';
import 'dart:developer';

import 'package:decidim_catraca_mobile/interfaces/response_interface.dart';
import 'package:decidim_catraca_mobile/models/admin/ambassadors.dart';
import 'package:decidim_catraca_mobile/models/admin/event_participant.dart';
import 'package:decidim_catraca_mobile/views/admin/home/admin_home_repository.dart';
import 'package:decidim_catraca_mobile/views/admin/home/components/device_card.dart';
import 'package:flutter/material.dart';
import 'package:flutter_blue/flutter_blue.dart';
import 'package:mobile_scanner/mobile_scanner.dart';
import 'package:collection/collection.dart';

enum PageState { LOADING, LOADED, ERROR }

enum PageSelected { EVENTS, HOME, QR }

class AdminHomeController {
  TextEditingController searchController = TextEditingController();
  ValueNotifier<String> searchText = ValueNotifier("");
  ValueNotifier<PageState> pageState = ValueNotifier(PageState.LOADING);
  ValueNotifier<PageSelected> page = ValueNotifier(PageSelected.HOME);

  ValueNotifier<bool> showGif = ValueNotifier(false);
  bool errorGif = true;
  Function gifFunction = () {};

  List<AmbassadorModel> ambassadors = [];

  List<AmbassadorModel> filteredAmbassadors = [];

  //Repository
  AdminHomeRepository repository = AdminHomeRepository();

  bool isLiberado = true;
  int liberados = 0;

  // Bluetooth Connection
  ValueNotifier<List<BluetoothDevice>> devicesList = ValueNotifier([]);
  final FlutterBlue flutterBlue = FlutterBlue.instance;
  ValueNotifier<BluetoothDevice?> deviceConnected = ValueNotifier(null);
  List<BluetoothService> _services = [];
  BluetoothCharacteristic? characteristic;
  bool sendingMsg = false;

  setStopScan() {
    page.addListener(() {
      if (page.value != PageSelected.HOME) {
        log("Stopping Scanning");
        flutterBlue.stopScan();
      }
    });
  }

  _addDeviceTolist(final BluetoothDevice device) {
    if (!devicesList.value.contains(device)) {
      devicesList.value.add(device);
      // ignore: invalid_use_of_protected_member, invalid_use_of_visible_for_testing_member
      devicesList.notifyListeners();
    }
  }

  startBluetoothScan() {
    flutterBlue.connectedDevices
        .asStream()
        .listen((List<BluetoothDevice> devices) {
      for (BluetoothDevice device in devices) {
        _addDeviceTolist(device);
        device.state.listen((BluetoothDeviceState state) {
          if (device == deviceConnected.value) {
            if (state == BluetoothDeviceState.disconnected) {
              deviceConnected.value == null;
              connectDevice(device);
            }
          }
        });
      }
    });
    flutterBlue.scanResults.listen((List<ScanResult> results) {
      for (ScanResult result in results) {
        if (!devicesList.value.contains(result.device) &&
            result.device.name != "") {
          _addDeviceTolist(result.device);
        }
      }
    });
    flutterBlue.startScan();
  }

  connectDevice(BluetoothDevice device) async {
    try {
      await device.connect();
    } catch (e) {
      log("Error connecting to device");
    } finally {
      _services = await device.discoverServices();

      for (BluetoothService element in _services) {
        if (element.uuid == Guid("6E400001-B5A3-F393-E0A9-E50E24DCCA9E")) {
          for (BluetoothCharacteristic char in element.characteristics) {
            if (char.uuid == Guid("6E400002-B5A3-F393-E0A9-E50E24DCCA9E")) {
              characteristic = char;
            }
          }
        }
      }
    }

    deviceConnected.value = device;
  }

  controlMsgSent(
      BarcodeCapture capture, List<EventParticipant> eventParticipants) async {
    if (!sendingMsg) {
      final List<Barcode> barcodes = capture.barcodes;
      EventParticipant? user;

      Barcode barcode = barcodes[0];
      user = eventParticipants
          .firstWhereOrNull((element) => element.id == barcode.rawValue);

      if (user != null) {
        errorGif = false;
        showGif.value = true;
        await sendBluetoothMessage(user.fullName, true);
      } else {
        errorGif = true;
        showGif.value = true;
        await sendBluetoothMessage("Sem cadastro", false);
      }
    }
  }

  sendBluetoothMessage(String userName, bool isLiberado) async {
    sendingMsg = true;
    log("Called");
    // if (deviceConnected.value != null) {
    //   await connectDevice(deviceConnected.value!);
    // }
    if (deviceConnected.value != null && characteristic != null) {
      String msg = "$userName ${isLiberado ? 1 : 0}";
      await characteristic!.write(utf8.encode(msg));
      log("Mensagem enviada para catraca - Usuário ${isLiberado ? "Liberado" : "Não Liberado"}");
    }
    sendingMsg = false;
  }

  getUsers() async {
    ResponseInterface response = await repository.getUsersRequest();

    if (response.data == null) {
      pageState.value = PageState.ERROR;
    } else {
      ambassadors = [];
      List<dynamic> users = response.data;
      for (Map<dynamic, dynamic> user in users) {
        ambassadors.add(AmbassadorModel.fromMap(user));
      }
      filteredAmbassadors = ambassadors;
      pageState.value = PageState.LOADED;
    }
  }

  filteredAmbassadorsList() {
    filteredAmbassadors = ambassadors;

    if (searchText.value != "") {
      filteredAmbassadors = filteredAmbassadors
          .where((element) =>
              element.fullName
                  .toLowerCase()
                  .contains(searchText.value.toLowerCase()) ||
              element.id.contains(searchText.value))
          .toList();
    }
  }

  updateSearchText() {
    searchText.value = searchController.text;
    filteredAmbassadorsList();
  }

  getNavbarAlignment() {
    if (page.value == PageSelected.HOME) {
      return Alignment.center;
    } else if (page.value == PageSelected.EVENTS) {
      return Alignment.centerLeft;
    } else {
      return Alignment.centerRight;
    }
  }

  List<Widget> buildDevicesList() {
    List<Widget> devices = [];

    for (BluetoothDevice element in devicesList.value) {
      devices.add(DeviceCard(device: element, controller: this));
    }
    return devices;
  }
}
