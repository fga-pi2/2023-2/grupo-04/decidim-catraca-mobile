import 'package:decidim_catraca_mobile/shared/app_styles.dart';
import 'package:decidim_catraca_mobile/views/admin/home/components/ambassador_card.dart';
import 'package:decidim_catraca_mobile/views/admin/home/controller/admin_home_controller.dart';
import 'package:flutter/material.dart';

import '../../../../components/admin_header.dart';

class AdminHomeArea extends StatefulWidget {
  const AdminHomeArea({super.key, required this.controller});

  final AdminHomeController controller;

  @override
  State<AdminHomeArea> createState() => _AdminHomeAreaState();
}

class _AdminHomeAreaState extends State<AdminHomeArea> {
  final border = OutlineInputBorder(
    borderSide: const BorderSide(
      color: Colors.black,
    ),
    borderRadius: BorderRadius.circular(40),
  );

  @override
  void initState() {
    widget.controller.getUsers();
    widget.controller.startBluetoothScan();
    widget.controller.setStopScan();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return SingleChildScrollView(
      child: ValueListenableBuilder(
        valueListenable: widget.controller.pageState,
        builder: (context, value, child) => value == PageState.LOADED
            ? Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const AdminHeader(showSignOutButton: true),
                  Padding(
                    padding:
                        EdgeInsets.symmetric(horizontal: size.width * 0.039),
                    child: const Text(
                      "Dispositivos Bluetooth",
                      style: AppStyles.pageTitle24,
                    ),
                  ),
                  SizedBox(
                    height: size.height * 0.15,
                    child: ValueListenableBuilder(
                      valueListenable: widget.controller.devicesList,
                      builder: (context, value, child) => ListView(
                        padding: const EdgeInsets.all(0),
                        children: widget.controller.buildDevicesList(),
                      ),
                    ),
                  ),
                  Padding(
                    padding:
                        EdgeInsets.symmetric(horizontal: size.width * 0.039),
                    child: const Text(
                      "Usuários",
                      style: AppStyles.pageTitle24,
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(top: size.height * 0.011),
                    padding:
                        EdgeInsets.symmetric(horizontal: size.width * 0.039),
                    child: TextField(
                      decoration: InputDecoration(
                        fillColor: Colors.black,
                        filled: true,
                        suffixIcon: const Icon(
                          Icons.search,
                          color: Colors.white,
                          size: 25,
                        ),
                        constraints:
                            BoxConstraints(maxHeight: size.height * 0.048),
                        contentPadding: EdgeInsets.only(
                          left: size.width * 0.05 + 25,
                          right: size.width * 0.02,
                        ),
                        border: border,
                        focusedBorder: border,
                        enabledBorder: border,
                        hintText: "Procure por nome ou id do usuário",
                        hintStyle: AppStyles.white13TitleStyle,
                      ),
                      cursorColor: Colors.white,
                      style: AppStyles.white13TitleStyle,
                      textAlign: TextAlign.center,
                      controller: widget.controller.searchController,
                      onChanged: (value) {
                        widget.controller.updateSearchText();
                      },
                    ),
                  ),
                  Scrollbar(
                    child: Container(
                      height: size.height * 0.4,
                      margin: EdgeInsets.only(top: size.height * 0.037),
                      padding:
                          EdgeInsets.symmetric(horizontal: size.width * 0.039),
                      child: ValueListenableBuilder(
                        valueListenable: widget.controller.searchText,
                        builder: (context, value, child) => Padding(
                          padding: EdgeInsets.only(bottom: size.height * 0.1),
                          child: ListView.builder(
                            padding: EdgeInsets.zero,
                            itemCount:
                                widget.controller.filteredAmbassadors.length,
                            itemBuilder: (context, index) => AmbassadorCard(
                              ambassador:
                                  widget.controller.filteredAmbassadors[index],
                              controller: widget.controller,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              )
            : const SizedBox(),
      ),
    );
  }
}
