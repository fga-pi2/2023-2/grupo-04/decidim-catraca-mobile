import 'package:decidim_catraca_mobile/models/admin/ambassadors.dart';
import 'package:decidim_catraca_mobile/shared/app_colors.dart';
import 'package:decidim_catraca_mobile/shared/app_styles.dart';
import 'package:decidim_catraca_mobile/views/admin/home/controller/admin_home_controller.dart';
import 'package:flutter/material.dart';

class AmbassadorCard extends StatelessWidget {
  const AmbassadorCard(
      {super.key, required this.ambassador, required this.controller});

  final AmbassadorModel ambassador;
  final AdminHomeController controller;

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      width: size.width * 0.913,
      margin: EdgeInsets.only(bottom: size.height * 0.027),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                ambassador.fullName,
                style: AppStyles.black20TitleStyle,
              ),
              Text(
                "#${ambassador.id}",
                style: AppStyles.black16Regular,
              )
              // Text(controller.searchText.value)
            ],
          ),
          Container(
            height: size.height * 0.043,
            width: size.width * 0.255,
            decoration: BoxDecoration(
              color: AppColors.primaryRed,
              borderRadius: BorderRadius.circular(40),
            ),
            child: ElevatedButton(
              onPressed: () {
                controller.sendBluetoothMessage(ambassador.fullName, true);
              },
              style: AppStyles.elevatedRedButtonStyle,
              child: const Center(
                child: Text(
                  "Liberar",
                  style: AppStyles.white13TitleStyle,
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}
