import 'package:decidim_catraca_mobile/shared/app_colors.dart';
import 'package:decidim_catraca_mobile/shared/app_styles.dart';
import 'package:decidim_catraca_mobile/views/admin/home/controller/admin_home_controller.dart';
import 'package:flutter/material.dart';
import 'package:flutter_blue/flutter_blue.dart';

class DeviceCard extends StatelessWidget {
  const DeviceCard({super.key, required this.device, required this.controller});

  final BluetoothDevice device;
  final AdminHomeController controller;

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      height: size.height * 0.04,
      margin: EdgeInsets.only(top: size.height * 0.01),
      padding: EdgeInsets.symmetric(horizontal: size.width * 0.08),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            device.name,
            style: const TextStyle(
                color: Colors.black, fontWeight: FontWeight.bold),
          ),
          ValueListenableBuilder(
            valueListenable: controller.deviceConnected,
            builder: (context, value, child) => Container(
              height: size.height * 0.043,
              width: size.width * 0.255,
              decoration: BoxDecoration(
                color: AppColors.primaryRed,
                borderRadius: BorderRadius.circular(40),
              ),
              child: ElevatedButton(
                onPressed: () async {
                  await controller.connectDevice(device);
                },
                style: AppStyles.elevatedRedButtonStyle,
                child: Center(
                  child: Text(
                    value == device ? "Desconectar" : "Conectar",
                    style: AppStyles.white13TitleStyle,
                  ),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}
