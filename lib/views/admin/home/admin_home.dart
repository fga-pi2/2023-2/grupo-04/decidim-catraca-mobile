import 'package:decidim_catraca_mobile/components/admin_nav_bar.dart';
import 'package:decidim_catraca_mobile/components/gif_page.dart';
import 'package:decidim_catraca_mobile/components/loading_page.dart';
import 'package:decidim_catraca_mobile/views/admin/events/events_page.dart';
import 'package:decidim_catraca_mobile/views/admin/home/admin_home_page.dart';
import 'package:decidim_catraca_mobile/views/admin/home/controller/admin_home_controller.dart';
import 'package:decidim_catraca_mobile/views/admin/qr_scanner/qr_code_scanner.dart';
import 'package:flutter/material.dart';

class AdminHomePage extends StatefulWidget {
  const AdminHomePage({super.key});

  @override
  State<AdminHomePage> createState() => _AdminHomePageState();
}

class _AdminHomePageState extends State<AdminHomePage> {
  AdminHomeController controller = AdminHomeController();

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: SizedBox(
        height: size.height,
        child: Stack(children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              ValueListenableBuilder(
                  valueListenable: controller.page,
                  builder: (context, value, child) {
                    if (value == PageSelected.HOME) {
                      return AdminHomeArea(controller: controller);
                    } else if (value == PageSelected.QR) {
                      return QRCodeScanner(homeController: controller);
                    } else {
                      return EventsAdminPage(homeController: controller);
                    }
                  })
            ],
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: AdminNavBar(
              alignSelected: Alignment.center,
              controller: controller,
            ),
          ),
          ValueListenableBuilder(
            valueListenable: controller.pageState,
            builder: (context, value, child) => value == PageState.LOADING
                ? const LoadingPage()
                : const SizedBox(),
          ),
          ValueListenableBuilder(
            valueListenable: controller.showGif,
            builder: (context, value, child) => value
                ? GifState(
                    error: controller.errorGif,
                    function: () {
                      controller.showGif.value = false;
                      controller.gifFunction();
                    },
                  )
                : const SizedBox(),
          ),
        ]),
      ),
      backgroundColor: Colors.white,
    );
  }
}
