import 'dart:developer';
import 'dart:io';

import 'package:decidim_catraca_mobile/interfaces/response_interface.dart';
import 'package:decidim_catraca_mobile/shared/app_constants.dart';
import 'package:dio/dio.dart';

class AdminHomeRepository {
  final dio = Dio(BaseOptions(
    baseUrl: AppConstants.BASE_URL,
    connectTimeout: const Duration(seconds: 15),
  ));

  Future<ResponseInterface> getUsersRequest() async {
    Response response;
    try {
      response = await dio.get(
        "/users",
        options: Options(
          headers: {
            HttpHeaders.contentTypeHeader: "application/json",
            HttpHeaders.authorizationHeader: AppConstants.actualUser.token,
          },
        ),
      );
      log("GET Users OK");
      return ResponseInterface(response.data["users"], 200);
    } on DioException catch (error) {
      if (error.response == null) {
        log("Problem with server");
        return ResponseInterface(null, 500);
      }
    }
    log("Problem with server");
    return ResponseInterface(null, 500);
  }
}
