import 'package:decidim_catraca_mobile/models/admin/event_participant.dart';
import 'package:decidim_catraca_mobile/shared/app_styles.dart';
import 'package:decidim_catraca_mobile/views/admin/home/controller/admin_home_controller.dart';
import 'package:decidim_catraca_mobile/views/admin/qr_scanner/qr_scanner_controller.dart';
import 'package:flutter/material.dart';
import 'package:mobile_scanner/mobile_scanner.dart';
import 'package:localstorage/localstorage.dart';

class QRCodeScanner extends StatefulWidget {
  const QRCodeScanner({super.key, required this.homeController});

  final AdminHomeController homeController;

  @override
  State<QRCodeScanner> createState() => _QRCodeScannerState();
}

class _QRCodeScannerState extends State<QRCodeScanner> {
  MobileScannerController cameraController = MobileScannerController();
  QRCodeScannerController controller = QRCodeScannerController();
  LocalStorage storage = LocalStorage("eventParticipants.json");
  dynamic storageParticipants;
  List<EventParticipant> eventParticipants = List.empty();

  @override
  void dispose() {
    cameraController.stop();
    cameraController.dispose();
    super.dispose();
  }

  @override
  void initState() {
    cameraController.stop();
    controller.getEvents();
    storageParticipants = storage.getItem("eventParticipants");

    if (storageParticipants != null) {
      eventParticipants = List<EventParticipant>.from(
        (storageParticipants as List).map(
          (participant) => EventParticipant(
            participant['id'],
            participant['fullName'],
            participant['email'],
            participant['phone'],
          ),
        ),
      );
    }

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Stack(
      children: [
        SafeArea(
          child: ValueListenableBuilder(
            valueListenable: controller.pageState,
            builder: (context, value, child) => value == QRPageState.LOADED
                ? Container(
                    width: size.width,
                    margin: EdgeInsets.only(top: size.height * 0.04),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        const Padding(
                          padding: EdgeInsets.symmetric(horizontal: 10),
                          child: Text(
                            "Posicione o QR Code na área marcada",
                            style: AppStyles.black20TitleStyle,
                            textAlign: TextAlign.center,
                          ),
                        ),
                        const Text(
                          "O escanemanto será feito automaticamente",
                          style: TextStyle(
                            color: Color(0XFFD9D9D9),
                            fontWeight: FontWeight.bold,
                            fontSize: 15,
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(top: size.height * 0.08),
                          child: Stack(
                            children: [
                              Center(
                                child: SizedBox(
                                  width: size.width * 0.95,
                                  height: size.height * 0.45,
                                  child: Center(
                                    child: SizedBox(
                                      width: size.width * 0.9,
                                      height: size.height * 0.4,
                                      child: MobileScanner(
                                        controller: cameraController,
                                        onDetect: (capture) async {
                                          await widget.homeController
                                              .controlMsgSent(
                                                  capture, eventParticipants);
                                        },
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                              Center(
                                child: SizedBox(
                                  width: size.width * 0.95,
                                  height: size.height * 0.45,
                                  child: Image.asset(
                                    "assets/images/qrcode.png",
                                    fit: BoxFit.fill,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                        Container(
                          width: size.width * 0.1,
                          height: size.height * 0.1,
                          decoration: BoxDecoration(
                            color: Colors.black,
                            shape: BoxShape.circle,
                            boxShadow: [
                              BoxShadow(
                                color: Colors.black.withOpacity(.4),
                                offset: const Offset(0, 4),
                                blurRadius: 6,
                                spreadRadius: 0,
                              )
                            ],
                          ),
                          child: Center(
                            child: IconButton(
                              color: Colors.white,
                              padding: EdgeInsets.zero,
                              icon: ValueListenableBuilder(
                                valueListenable: cameraController.torchState,
                                builder: (context, state, child) {
                                  switch (state) {
                                    case TorchState.off:
                                      return const Icon(
                                        Icons.flash_off,
                                        color: Colors.white,
                                      );
                                    case TorchState.on:
                                      return const Icon(
                                        Icons.flash_on,
                                        color: Colors.yellow,
                                      );
                                  }
                                },
                              ),
                              iconSize: 22.0,
                              onPressed: () => cameraController.toggleTorch(),
                            ),
                          ),
                        ),
                      ],
                    ),
                  )
                : value == QRPageState.LOADING
                    ? SizedBox(
                        height: size.height * 0.8,
                        width: size.width,
                        child: const Center(
                          child: CircularProgressIndicator(),
                        ),
                      )
                    : const SizedBox(),
          ),
        ),
      ],
    );
  }
}
