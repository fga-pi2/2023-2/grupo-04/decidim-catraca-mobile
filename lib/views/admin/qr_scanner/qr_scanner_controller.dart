// ignore_for_file: constant_identifier_names

import 'package:decidim_catraca_mobile/interfaces/response_interface.dart';
import 'package:decidim_catraca_mobile/models/admin/events_list.dart';
import 'package:decidim_catraca_mobile/models/event.dart';
import 'package:decidim_catraca_mobile/views/admin/events/events_page_repository.dart';
import 'package:flutter/material.dart';

enum QRPageState { LOADING, LOADED, ERROR }

class QRCodeScannerController {
  EventsRepository repository = EventsRepository();
  ValueNotifier<QRPageState> pageState = ValueNotifier(QRPageState.LOADING);
  List<EventsList> listEvents = [];
  List<EventModel> eventModelList = [];

  EventModel? eventSelected;

  getEvents() async {
    ResponseInterface response = await repository.getEventsRequest();

    if (response.data == null) {
      pageState.value = QRPageState.ERROR;
    } else {
      List<dynamic> events = response.data;
      for (Map<dynamic, dynamic> dayOfEvent in events) {
        listEvents.add(EventsList.fromMap(dayOfEvent));
      }
      for (EventsList event in listEvents) {
        eventModelList.addAll(event.events);
      }

      eventModelList = eventModelList.reversed.toList();
      pageState.value = QRPageState.LOADED;
    }
  }

  Future<bool> sendAmbassadorPresence(String? ambassadorId, int eventId) async {
    if (ambassadorId == null) return false;
    ResponseInterface response =
        await repository.sendAmbassadorPresence(ambassadorId, eventId);

    if (response.statusCode == 500) {
      return false;
    }
    return true;
  }
}
