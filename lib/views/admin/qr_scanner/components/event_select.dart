import 'package:decidim_catraca_mobile/models/event.dart';
import 'package:decidim_catraca_mobile/shared/app_styles.dart';
import 'package:decidim_catraca_mobile/views/admin/qr_scanner/qr_scanner_controller.dart';
import 'package:dropdown_button2/dropdown_button2.dart';
import 'package:flutter/material.dart';

class EventSelect extends StatefulWidget {
  const EventSelect(
      {super.key, required this.events, required this.qrCodeController});

  final List<EventModel> events;
  final QRCodeScannerController qrCodeController;

  @override
  State<EventSelect> createState() => _EventSelectState();
}

class _EventSelectState extends State<EventSelect> {
  @override
  void initState() {
    if (widget.events.isNotEmpty) {
      widget.qrCodeController.eventSelected = widget.events[0];
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      decoration: BoxDecoration(
        color: const Color(0XFFFF4E4E),
        borderRadius: BorderRadius.circular(5),
      ),
      margin: EdgeInsets.only(top: size.height * 0.03),
      child: DropdownButton2(
        items: widget.events
            .map<DropdownMenuItem<String>>(
              (EventModel event) => DropdownMenuItem(
                value: event.id.toString(),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      event.name,
                      style: AppStyles.white14TitleStyle,
                    ),
                    Text(
                      "${event.date} at ${event.eventSchedule[0].startTime}",
                      style: AppStyles.white12Regular,
                    ),
                  ],
                ),
              ),
            )
            .toList(),
        value: widget.qrCodeController.eventSelected!.id.toString(),
        onChanged: (String? value) {
          setState(() {
            widget.qrCodeController.eventSelected = widget.events
                .where((element) => element.id == int.parse(value ?? "0"))
                .first;
          });
        },
        isExpanded: true,
        buttonStyleData: ButtonStyleData(
          decoration: BoxDecoration(
            border: Border.all(
              color: Colors.transparent,
              width: 0,
            ),
            borderRadius: BorderRadius.circular(5),
          ),
          height: size.height * 0.0611,
          width: size.width * 0.75,
          padding: EdgeInsets.only(
            left: size.width * 0.048,
            top: size.height * 0.0075,
          ),
        ),
        style: AppStyles.inputLabelStyle,
        underline: const SizedBox(),
        menuItemStyleData: MenuItemStyleData(
          height: size.height * 0.0511,
        ),
        dropdownStyleData: DropdownStyleData(
          maxHeight: size.height * 0.4,
          padding: const EdgeInsets.only(bottom: 0, top: 5),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(5),
            color: const Color(0XFFFF4E4E),
          ),
        ),
      ),
    );

    // return Container(
    //   height: size.height * 0.0611,
    //   width: size.width * 0.75,
    //   margin: EdgeInsets.only(top: size.height * 0.03),
    //   padding: EdgeInsets.only(
    //     left: size.width * 0.048,
    //     top: size.height * 0.0075,
    //   ),
    //   decoration: BoxDecoration(
    //     color: const Color(0XFFFF4E4E),
    //     borderRadius: BorderRadius.circular(5),
    //   ),
    // child: Column(
    //   crossAxisAlignment: CrossAxisAlignment.start,
    //   children: [
    //     Text(
    //       actualEvent.name,
    //       style: AppStyles.white14TitleStyle,
    //     ),
    //     Text(
    //       "${actualEvent.date} at ${actualEvent.eventSchedule[0].startTime}",
    //       style: AppStyles.white12Regular,
    //     ),
    //   ],
    // ),
    // );
  }
}
