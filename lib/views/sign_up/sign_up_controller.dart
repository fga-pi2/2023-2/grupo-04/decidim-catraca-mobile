// ignore_for_file: constant_identifier_names

import 'package:decidim_catraca_mobile/interfaces/response_interface.dart';
import 'package:decidim_catraca_mobile/views/sign_up/sign_up_repository.dart';
import 'package:email_validator/email_validator.dart';
import 'package:flutter/material.dart';

enum PageState { LOADING, LOADED }

class SignUpController {
  // Fields Controllers
  TextEditingController fullNameController = TextEditingController();
  TextEditingController emailController = TextEditingController();
  TextEditingController phoneController = TextEditingController();
  TextEditingController passwordController = TextEditingController();

  // ValueNotifier
  ValueNotifier<bool> fullNameErrorMessage = ValueNotifier(false);
  ValueNotifier<bool> emailErrorMessage = ValueNotifier(false);
  ValueNotifier<bool> phoneErrorMessage = ValueNotifier(false);
  ValueNotifier<bool> passwordErrorMessage = ValueNotifier(false);

  ValueNotifier<PageState> pageState = ValueNotifier(PageState.LOADED);

  // Repository
  SignUpRepository repository = SignUpRepository();

  Future<bool> sendLogInRequest() async {
    pageState.value = PageState.LOADING;
    ResponseInterface response = await repository.signUpRequest(
        fullNameController.text,
        emailController.text,
        phoneController.text,
        passwordController.text);

    if (response.statusCode != 200) return false;

    return true;
  }

  bool validateFullName() {
    String? value = fullNameController.text;
    if (value.length < 4) {
      fullNameErrorMessage.value = true;
      return false;
    }
    fullNameErrorMessage.value = false;
    return true;
  }

  bool validateEmail() {
    String? value = emailController.text;
    if (!EmailValidator.validate(value)) {
      emailErrorMessage.value = true;
      return false;
    }
    emailErrorMessage.value = false;
    return true;
  }

  bool validatePhone() {
    if (phoneController.text.length != 15) {
      phoneErrorMessage.value = true;
      return false;
    }
    phoneErrorMessage.value = false;
    return true;
  }

  bool validatePassword() {
    String? value = passwordController.text;
    if (value.length < 6) {
      passwordErrorMessage.value = true;
      return false;
    }
    passwordErrorMessage.value = false;
    return true;
  }

  bool validateForm() {
    return validateFullName() &
        validateEmail() &
        validatePassword() &
        validatePhone();
  }
}
