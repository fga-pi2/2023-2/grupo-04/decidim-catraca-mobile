import 'dart:developer';
import 'dart:io';

import 'package:decidim_catraca_mobile/interfaces/response_interface.dart';
import 'package:decidim_catraca_mobile/shared/app_constants.dart';
import 'package:dio/dio.dart';

class SignUpRepository {
  final dio = Dio(BaseOptions(
    baseUrl: AppConstants.BASE_URL,
    connectTimeout: const Duration(seconds: 15),
  ));

  Future<ResponseInterface> signUpRequest(
      String fullName, String email, String phone, String password) async {
    Map<String, String> body = {
      "fullName": fullName,
      "email": email,
      "phone": phone,
      "password": password,
    };
    Response response;
    try {
      response = await dio.post(
        "/signup",
        options: Options(
          headers: {HttpHeaders.contentTypeHeader: "application/json"},
        ),
        data: body,
      );
      log("Register ok");
      return ResponseInterface(response.data, 200);
    } on DioException {
      log("Problem with server");
      return ResponseInterface(null, 500);
    }
  }
}
