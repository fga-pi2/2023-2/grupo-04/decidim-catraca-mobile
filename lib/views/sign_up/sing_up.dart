// ignore_for_file: prefer_const_constructors, use_build_context_synchronously

import 'package:decidim_catraca_mobile/components/custom_text_field.dart';
import 'package:decidim_catraca_mobile/components/loading_page.dart';
import 'package:decidim_catraca_mobile/components/server_error_dialog.dart';
import 'package:decidim_catraca_mobile/shared/shared.dart';
import 'package:decidim_catraca_mobile/views/sign_in/sign_in.dart';
import 'package:decidim_catraca_mobile/views/sign_up/sign_up_controller.dart';
import 'package:flutter/material.dart';
import 'package:masked_text/masked_text.dart';

class SignUpPage extends StatefulWidget {
  const SignUpPage({super.key});

  @override
  State<SignUpPage> createState() => _SignUpPageState();
}

class _SignUpPageState extends State<SignUpPage> {
  SignUpController controller = SignUpController();

  final normalBorder = const OutlineInputBorder(
    borderRadius: BorderRadius.all(
      Radius.circular(20),
    ),
    borderSide: BorderSide(
      color: Colors.black,
      width: 2,
    ),
  );

  final errorBorder = const OutlineInputBorder(
    borderRadius: BorderRadius.all(
      Radius.circular(20),
    ),
    borderSide: BorderSide(
      color: Colors.red,
      width: 2,
    ),
  );

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: Stack(
        children: [
          Container(
            width: size.width * 0.7,
            color: AppColors.primaryRed,
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.end,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                margin: EdgeInsets.only(
                  left: size.width * 0.058,
                  bottom: size.height * 0.006,
                ),
                child: const Text(
                  "Cadastro",
                  style: AppStyles.whiteTitleStyle,
                ),
              ),
              Container(
                height: size.height * 0.686,
                width: size.width,
                padding: EdgeInsets.only(
                  top: size.height * 0.049,
                  right: size.width * 0.072,
                  left: size.width * 0.072,
                ),
                decoration: const BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.vertical(
                    top: Radius.circular(30),
                  ),
                ),
                child: Column(
                  children: [
                    CustomTextField(
                      labelText: "Nome completo",
                      controller: controller.fullNameController,
                    ),
                    ValueListenableBuilder(
                      valueListenable: controller.fullNameErrorMessage,
                      builder: (context, value, child) => value
                          ? Container(
                              padding: EdgeInsets.only(left: size.width * 0.05),
                              margin: EdgeInsets.only(top: size.height * 0.005),
                              child: const Align(
                                alignment: Alignment.centerLeft,
                                child: Text(
                                  "Nome muito curto!",
                                  style: AppStyles.red10TitleStyle,
                                ),
                              ),
                            )
                          : const SizedBox(),
                    ),
                    Container(
                      margin: EdgeInsets.only(
                        top: size.height * 0.016,
                      ),
                      child: CustomTextField(
                        labelText: "Email",
                        controller: controller.emailController,
                      ),
                    ),
                    ValueListenableBuilder(
                      valueListenable: controller.emailErrorMessage,
                      builder: (context, value, child) => value
                          ? Container(
                              padding: EdgeInsets.only(left: size.width * 0.05),
                              margin: EdgeInsets.only(top: size.height * 0.005),
                              child: const Align(
                                alignment: Alignment.centerLeft,
                                child: Text(
                                  "Email inválido",
                                  style: AppStyles.red10TitleStyle,
                                ),
                              ),
                            )
                          : const SizedBox(),
                    ),
                    Container(
                      margin: EdgeInsets.only(
                        top: size.height * 0.016,
                      ),
                      child: MaskedTextField(
                        controller: controller.phoneController,
                        decoration: InputDecoration(
                          focusedBorder: normalBorder,
                          enabledBorder: normalBorder,
                          errorBorder: errorBorder,
                          focusedErrorBorder: errorBorder,
                          labelText: "Telefone",
                          labelStyle: AppStyles.inputLabelStyle,
                          constraints:
                              BoxConstraints(maxHeight: size.height * 0.048),
                          helperText: null,
                          contentPadding: EdgeInsets.only(
                            left: size.width * 0.05,
                            right: size.width * 0.02,
                          ),
                          errorStyle: AppStyles.red10TitleStyle,
                        ),
                        keyboardType: TextInputType.number,
                        cursorColor: Colors.black,
                        cursorHeight: 15,
                        expands: false,
                        mask: "(##) #####-####",
                      ),
                    ),
                    ValueListenableBuilder(
                      valueListenable: controller.phoneErrorMessage,
                      builder: (context, value, child) => value
                          ? Container(
                              padding: EdgeInsets.only(left: size.width * 0.05),
                              margin: EdgeInsets.only(top: size.height * 0.005),
                              child: const Align(
                                alignment: Alignment.centerLeft,
                                child: Text(
                                  "Telefone inválido",
                                  style: AppStyles.red10TitleStyle,
                                ),
                              ),
                            )
                          : const SizedBox(),
                    ),
                    Container(
                      margin: EdgeInsets.only(
                        top: size.height * 0.016,
                      ),
                      child: CustomTextField(
                        labelText: "Senha",
                        obscureText: true,
                        controller: controller.passwordController,
                      ),
                    ),
                    ValueListenableBuilder(
                      valueListenable: controller.passwordErrorMessage,
                      builder: (context, value, child) => value
                          ? Container(
                              padding: EdgeInsets.only(left: size.width * 0.05),
                              margin: EdgeInsets.only(top: size.height * 0.005),
                              child: const Align(
                                alignment: Alignment.centerLeft,
                                child: Text(
                                  "Sua senha deve conter 6 caracteres",
                                  style: AppStyles.red10TitleStyle,
                                ),
                              ),
                            )
                          : const SizedBox(),
                    ),
                    Container(
                      height: size.height * 0.046,
                      width: size.width,
                      margin: EdgeInsets.only(
                        top: size.height * 0.046,
                      ),
                      decoration: BoxDecoration(
                        color: AppColors.primaryRed,
                        borderRadius: BorderRadius.circular(20),
                      ),
                      child: ElevatedButton(
                        onPressed: () async {
                          if (controller.validateForm()) {
                            bool registerOk =
                                await controller.sendLogInRequest();
                            controller.pageState.value = PageState.LOADED;
                            if (registerOk) {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => SignInPage(),
                                ),
                              );
                            } else {
                              showDialog(
                                context: context,
                                builder: (context) => ServerErrorDialog(),
                              );
                            }
                          }
                        },
                        style: AppStyles.elevatedRedButtonStyle,
                        child: const Center(
                          child: Text(
                            "Cadastrar",
                            style: AppStyles.white16ButtonTitle,
                          ),
                        ),
                      ),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        const Text(
                          "Já possui uma conta ? ",
                          style: AppStyles.black13TitleStyle,
                        ),
                        ElevatedButton(
                          onPressed: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => const SignInPage(),
                                ));
                          },
                          style: AppStyles.elevatedTextButton,
                          child: const Text(
                            "Entrar",
                            style: AppStyles.red13TitleStyle,
                          ),
                        ),
                      ],
                    ),
                    Container(
                      height: size.height * 0.094,
                      width: size.width * 0.39,
                      margin: EdgeInsets.only(top: size.height * 0.1),
                      child: Image.asset(
                        "assets/images/decidim-footer.png",
                      ),
                    )
                  ],
                ),
              )
            ],
          ),
          SizedBox(
            height: size.height * 0.343,
            width: size.width,
            child: Image.asset(
              "assets/images/sign_in/black_header.png",
              fit: BoxFit.fitWidth,
            ),
          ),
          Positioned(
            top: size.height * 0.07,
            right: -size.width * 0.45,
            left: 0,
            child: Center(
              child: SizedBox(
                width: size.width * 0.642,
                height: size.height * 0.204,
                child: Image.asset("assets/images/decidim-logo.png"),
              ),
            ),
          ),
          ValueListenableBuilder(
            valueListenable: controller.pageState,
            builder: (context, value, child) =>
                value == PageState.LOADING ? LoadingPage() : SizedBox(),
          )
        ],
      ),
      backgroundColor: Colors.black,
    );
  }
}
