// ignore_for_file: constant_identifier_names

import 'dart:developer';

import 'package:decidim_catraca_mobile/interfaces/response_interface.dart';
import 'package:decidim_catraca_mobile/models/user_model.dart';
import 'package:decidim_catraca_mobile/shared/app_constants.dart';
import 'package:decidim_catraca_mobile/views/sign_in/sign_in_repository.dart';
import 'package:email_validator/email_validator.dart';
import 'package:flutter/material.dart';

enum PageState { LOADING, LOADED }

class SignInController {
  // Fields Controllers
  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();

  // ValueNotifier
  ValueNotifier<bool> emailErrorMessage = ValueNotifier(false);
  ValueNotifier<bool> passwordErrorMessage = ValueNotifier(false);
  ValueNotifier<PageState> pageState = ValueNotifier(PageState.LOADED);

  // Repository
  SignInRepository repository = SignInRepository();

  Future<int> sendLogInRequest() async {
    pageState.value = PageState.LOADING;
    ResponseInterface response = await repository.signInRequest(
      emailController.text,
      passwordController.text,
    );

    if (response.statusCode == 404) {
      emailErrorMessage.value = true;
      passwordErrorMessage.value = true;
      pageState.value = PageState.LOADED;
      return 404;
    } else if (response.statusCode == 500) {
      pageState.value = PageState.LOADED;
      return 500;
    } else {
      UserModel user = UserModel.fromMap(response.data, emailController.text);
      AppConstants.actualUser = user;
      log("Log in done");
      log(user.token);
      pageState.value = PageState.LOADED;
      return 200;
    }
  }

  bool validatePassword() {
    String? value = passwordController.text;
    if (value.length < 6) {
      passwordErrorMessage.value = true;
      return false;
    }
    passwordErrorMessage.value = false;
    return true;
  }

  bool validateEmail() {
    String? value = emailController.text;
    if (!EmailValidator.validate(value)) {
      emailErrorMessage.value = true;
      return false;
    }
    emailErrorMessage.value = false;
    return true;
  }

  String getPasswordErrorMessage() {
    if (passwordController.text.length < 6) {
      return "Sua senha deve conter no mínimo 6 caracteres";
    }
    return "Email ou senha inválido!";
  }

  String getEmailErrorMessage() {
    if (EmailValidator.validate(emailController.text)) {
      return "Email ou senha inválido!";
    }
    return "Email inválido!";
  }
}
