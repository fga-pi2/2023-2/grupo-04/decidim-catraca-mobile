// ignore_for_file: prefer_const_constructors, use_build_context_synchronously

import 'package:decidim_catraca_mobile/components/custom_text_field.dart';
import 'package:decidim_catraca_mobile/components/loading_page.dart';
import 'package:decidim_catraca_mobile/components/server_error_dialog.dart';
import 'package:decidim_catraca_mobile/shared/app_colors.dart';
import 'package:decidim_catraca_mobile/shared/app_styles.dart';
import 'package:decidim_catraca_mobile/views/admin/home/admin_home.dart';
import 'package:decidim_catraca_mobile/views/sign_in/sign_in_controller.dart';
import 'package:decidim_catraca_mobile/views/sign_up/sing_up.dart';
import 'package:decidim_catraca_mobile/views/user/home/user_home.dart';
import 'package:flutter/material.dart';

import '../../shared/app_constants.dart';

class SignInPage extends StatefulWidget {
  const SignInPage({super.key});

  @override
  State<SignInPage> createState() => _SignInPageState();
}

class _SignInPageState extends State<SignInPage> {
  SignInController controller = SignInController();

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: SafeArea(
        bottom: false,
        child: Stack(
          children: [
            Container(
              width: size.width * 0.7,
              color: AppColors.primaryRed,
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.end,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  margin: EdgeInsets.only(
                    left: size.width * 0.058,
                    bottom: size.height * 0.006,
                  ),
                  child: const Text(
                    "Entrar",
                    style: AppStyles.whiteTitleStyle,
                  ),
                ),
                Container(
                  height: size.height * 0.626,
                  width: size.width,
                  padding: EdgeInsets.only(
                    top: size.height * 0.079,
                    right: size.width * 0.072,
                    left: size.width * 0.072,
                  ),
                  decoration: const BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.vertical(
                      top: Radius.circular(30),
                    ),
                  ),
                  child: Column(
                    children: [
                      CustomTextField(
                        labelText: "Email",
                        controller: controller.emailController,
                      ),
                      ValueListenableBuilder(
                        valueListenable: controller.emailErrorMessage,
                        builder: (context, value, child) => value
                            ? Container(
                                padding:
                                    EdgeInsets.only(left: size.width * 0.05),
                                margin:
                                    EdgeInsets.only(top: size.height * 0.01),
                                child: Align(
                                  alignment: Alignment.centerLeft,
                                  child: Text(
                                    controller.getEmailErrorMessage(),
                                    style: AppStyles.red10TitleStyle,
                                  ),
                                ),
                              )
                            : const SizedBox(),
                      ),
                      Container(
                        margin: EdgeInsets.only(
                          top: size.height * 0.026,
                        ),
                        child: CustomTextField(
                          labelText: "Senha",
                          obscureText: true,
                          controller: controller.passwordController,
                        ),
                      ),
                      ValueListenableBuilder(
                        valueListenable: controller.passwordErrorMessage,
                        builder: (context, value, child) => value
                            ? Container(
                                padding:
                                    EdgeInsets.only(left: size.width * 0.05),
                                margin:
                                    EdgeInsets.only(top: size.height * 0.01),
                                child: Align(
                                  alignment: Alignment.centerLeft,
                                  child: Text(
                                    controller.getPasswordErrorMessage(),
                                    style: AppStyles.red10TitleStyle,
                                  ),
                                ),
                              )
                            : const SizedBox(),
                      ),
                      // Align(
                      //   alignment: Alignment.centerRight,
                      //   child: ElevatedButton(
                      //     onPressed: () {
                      //       // Navigator.push(
                      //       //   context,
                      //       //   MaterialPageRoute(
                      //       //     builder: (context) => ResetPassword(),
                      //       //   ),
                      //       // );
                      //     },
                      //     style: AppStyles.elevatedTextButton,
                      //     child: const Text(
                      //       "Esqueceu a senha?",
                      //       style: AppStyles.black10TitleStyle,
                      //     ),
                      //   ),
                      // ),
                      Container(
                        height: size.height * 0.056,
                        width: size.width,
                        margin: EdgeInsets.only(
                          top: size.height * 0.046,
                        ),
                        decoration: BoxDecoration(
                          color: AppColors.primaryRed,
                          borderRadius: BorderRadius.circular(20),
                        ),
                        child: ElevatedButton(
                          onPressed: () async {
                            if (controller.validateEmail() &&
                                controller.validatePassword()) {
                              int statusCode =
                                  await controller.sendLogInRequest();

                              if (statusCode == 200) {
                                if (AppConstants.actualUser.isAdmin) {
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                      builder: (context) => AdminHomePage(),
                                    ),
                                  );
                                } else {
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                      builder: (context) => UserHomePage(),
                                    ),
                                  );
                                }
                              } else if (statusCode == 500) {
                                showDialog(
                                  context: context,
                                  builder: (context) => ServerErrorDialog(),
                                );
                              }
                            }
                          },
                          style: AppStyles.elevatedRedButtonStyle,
                          child: const Center(
                            child: Text(
                              "Entrar",
                              style: AppStyles.white20ButtonTitle,
                            ),
                          ),
                        ),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          const Text(
                            "Não tem uma conta ? ",
                            style: AppStyles.black13TitleStyle,
                          ),
                          ElevatedButton(
                            onPressed: () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => const SignUpPage(),
                                ),
                              );
                            },
                            style: AppStyles.elevatedTextButton,
                            child: const Text(
                              "Cadastre-se",
                              style: AppStyles.red13TitleStyle,
                            ),
                          ),
                        ],
                      ),
                      Expanded(
                        child: SafeArea(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              SizedBox(
                                height: size.height * 0.094,
                                width: size.width * 0.39,
                                child: Image.asset(
                                  "assets/images/decidim-footer.png",
                                ),
                              ),
                            ],
                          ),
                        ),
                      )
                    ],
                  ),
                )
              ],
            ),
            SizedBox(
              height: size.height * 0.343,
              width: size.width,
              child: Image.asset(
                "assets/images/sign_in/black_header.png",
                fit: BoxFit.contain,
              ),
            ),
            Positioned(
              top: 0,
              right: -size.width * 0.45,
              left: 0,
              child: Center(
                child: SizedBox(
                  width: size.width * 0.672,
                  height: size.height * 0.244,
                  child: Image.asset(
                    "assets/images/decidim-logo.png",
                  ),
                ),
              ),
            ),
            ValueListenableBuilder(
              valueListenable: controller.pageState,
              builder: (context, value, child) =>
                  value == PageState.LOADING ? LoadingPage() : SizedBox(),
            )
          ],
        ),
      ),
      backgroundColor: Colors.black,
    );
  }
}
