import 'dart:developer';
import 'dart:io';

import 'package:decidim_catraca_mobile/interfaces/response_interface.dart';
import 'package:dio/dio.dart';
import '../../shared/app_constants.dart';

class SignInRepository {
  final dio = Dio(BaseOptions(
    baseUrl: AppConstants.BASE_URL,
    connectTimeout: const Duration(seconds: 7),
  ));

  Future<ResponseInterface> signInRequest(String email, String password) async {
    Map<String, String> body = {"email": email, "password": password};
    Response response;
    try {
      response = await dio.post(
        "/login",
        options: Options(
          headers: {HttpHeaders.contentTypeHeader: "application/json"},
        ),
        data: body,
      );
      log("Login Ok");
      return ResponseInterface(response.data, 200);
    } on DioException catch (error) {
      if (error.response == null) {
        log("Problem connecting to host");
        return ResponseInterface(null, 500);
      } else if (error.response!.statusCode == 404) {
        log("User not found");
        return ResponseInterface(null, 404);
      }
    }
    log("Problem with server");
    return ResponseInterface(null, 500);
  }
}
