import 'package:flutter/material.dart';
import 'package:qr_flutter/qr_flutter.dart';

import '../../../shared/shared.dart';

class QrCodeArea extends StatelessWidget {
  const QrCodeArea({
    super.key,
    required this.name,
    required this.id,
  });

  final String name;
  final String id;

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return SizedBox(
      width: size.width,
      child: Column(children: [
        Text(name, style: AppStyles.black32Regular),
        Text("#$id", style: AppStyles.black32Regular),
        Container(
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(30), color: Colors.black),
          height: size.height * 0.44,
          width: size.width * 0.85,
          margin: EdgeInsets.only(top: size.height * 0.02),
          child: Center(
            child: QrImageView(
              padding: const EdgeInsets.all(40),
              data: id,
              version: 1,
              eyeStyle: const QrEyeStyle(
                  color: Colors.white, eyeShape: QrEyeShape.square),
              dataModuleStyle: const QrDataModuleStyle(
                  color: Colors.white,
                  dataModuleShape: QrDataModuleShape.square),
              gapless: false,
            ),
          ),
        )
      ]),
    );
  }
}
