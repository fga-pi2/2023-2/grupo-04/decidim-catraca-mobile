import 'package:decidim_catraca_mobile/views/admin/events/events_page_controller.dart';
import 'package:flutter/material.dart';

import '../../../components/day_events_card.dart';
import '../../../shared/shared.dart';

class NewEvents extends StatefulWidget {
  const NewEvents({super.key});

  @override
  State<NewEvents> createState() => _NewEventsState();
}

class _NewEventsState extends State<NewEvents> {
  final EventsPageController controller = EventsPageController();

  @override
  void initState() {
    controller.getEvents();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Column(
      children: [
        Padding(
          padding: EdgeInsets.symmetric(horizontal: size.width * 0.039),
          child: const Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                "Próximos eventos",
                style: AppStyles.pageTitle24,
              ),
            ],
          ),
        ),
        ValueListenableBuilder(
          valueListenable: controller.pageState,
          builder: (context, value, child) => value == PageEventState.LOADED
              ? Container(
                  height: size.height * 0.6,
                  margin: EdgeInsets.only(top: size.height * 0.02),
                  child: controller.listEvents.isNotEmpty
                      ? ListView.builder(
                          padding: EdgeInsets.zero,
                          itemCount: controller.listEvents.length,
                          itemBuilder: (context, index) => DayEventsCard(
                            weekDay: controller.listEvents[index].weekDay,
                            day: controller.listEvents[index].day,
                            month: controller.listEvents[index].month,
                            eventsList: controller.listEvents[index].events,
                            isEditEvent: false,
                          ),
                        )
                      : Align(
                          alignment: Alignment.topCenter,
                          child: Column(
                            children: [
                              Container(
                                height: size.height * 0.25,
                                width: size.width * 0.6,
                                margin: EdgeInsets.only(
                                  top: size.height * 0.08,
                                ),
                                child: Image.asset(
                                  "assets/images/empty-events.png",
                                ),
                              ),
                              const Text(
                                "Não existe nenhum evento agendado!",
                                style: AppStyles.black12semiBold,
                              )
                            ],
                          ),
                        ),
                )
              : value == PageEventState.ERROR
                  ? SizedBox(
                      height: size.height * 0.5,
                      width: size.width,
                      child: const Center(
                        child: Text(
                          "Desculpe! Estamos enfrentando problema com o servidor",
                          style: AppStyles.black20TitleStyle,
                          textAlign: TextAlign.center,
                        ),
                      ),
                    )
                  : Container(
                      margin: EdgeInsets.only(top: size.height * 0.3),
                      child: const Center(
                        child: CircularProgressIndicator(),
                      ),
                    ),
        )
      ],
    );
  }
}
