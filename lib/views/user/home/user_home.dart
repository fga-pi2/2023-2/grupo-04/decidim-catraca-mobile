import 'package:decidim_catraca_mobile/components/user_header.dart';
import 'package:decidim_catraca_mobile/components/user_nav_bar.dart';
import 'package:decidim_catraca_mobile/shared/app_constants.dart';
import 'package:decidim_catraca_mobile/views/user/events/latest_events_page.dart';
import 'package:decidim_catraca_mobile/views/user/events/new_events_page.dart';
import 'package:decidim_catraca_mobile/views/user/home/user_home_controller.dart';
import 'package:decidim_catraca_mobile/views/user/qr_code/qr_code_area.dart';
import 'package:flutter/material.dart';

class UserHomePage extends StatefulWidget {
  const UserHomePage({super.key});

  @override
  State<UserHomePage> createState() => _UserHomePageState();
}

class _UserHomePageState extends State<UserHomePage> {
  final border = OutlineInputBorder(
    borderSide: const BorderSide(
      color: Colors.black,
    ),
    borderRadius: BorderRadius.circular(40),
  );
  final UserHomeController _userHomeController = UserHomeController();
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      body: SizedBox(
        height: size.height,
        child: Stack(children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const UserHeader(showSignOutButton: true),
              ValueListenableBuilder(
                  valueListenable: _userHomeController.pageNotifier,
                  builder: (context, value, child) {
                    if (value == "HOME") {
                      return const NewEvents();
                    } else if (value == "QRCODE") {
                      return QrCodeArea(
                        id: AppConstants.actualUser.id,
                        name: AppConstants.actualUser.fullName,
                      );
                    } else {
                      return const LastEvents();
                    }
                  })
            ],
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: UserNavBar(
              alignSelected: Alignment.center,
              homeUserController: _userHomeController,
            ),
          ),
        ]),
      ),
      backgroundColor: Colors.white,
    );
  }
}
