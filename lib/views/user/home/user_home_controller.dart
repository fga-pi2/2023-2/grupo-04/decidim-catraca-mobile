import 'package:flutter/material.dart';

class UserHomeController {
  ValueNotifier<String> pageNotifier = ValueNotifier("HOME");

  getNavbarAlignment() {
    if (pageNotifier.value == "HOME") {
      return Alignment.center;
    } else if (pageNotifier.value == "CALENDAR") {
      return Alignment.centerLeft;
    } else {
      return Alignment.centerRight;
    }
  }
}
