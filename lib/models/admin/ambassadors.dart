class AmbassadorModel {
  final String id;
  final String fullName;
  final String email;
  final String phone;

  AmbassadorModel(
    this.id,
    this.fullName,
    this.email,
    this.phone,
  );

  factory AmbassadorModel.fromMap(Map<dynamic, dynamic> data) {
    List<String> namesList = data["fullName"].split(" ");
    String name = "${namesList.first} ${namesList.last}";
    return AmbassadorModel(
      data["id"],
      name,
      data["email"],
      data["phone"],
    );
  }
}
