class EventParticipant {
  final String id;
  final String fullName;
  final String email;
  final String phone;

  EventParticipant(this.id, this.fullName, this.email, this.phone);

  toJSONEncodable() {
    Map<String, dynamic> m = {};

    m['id'] = id;
    m['fullName'] = fullName;
    m['email'] = email;
    m['phone'] = phone;

    return m;
  }

  factory EventParticipant.fromMap(Map<dynamic, dynamic> data) {
    List<String> namesList = data["fullName"].split(" ");
    String name = "${namesList.first} ${namesList.last}";
    return EventParticipant(
      data["id"],
      name,
      data["email"],
      data["phone"],
    );
  }
}
