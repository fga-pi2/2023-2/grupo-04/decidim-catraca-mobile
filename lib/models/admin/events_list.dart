import 'package:decidim_catraca_mobile/models/event.dart';

class EventsList {
  final String weekDay;
  final String day;
  final String month;
  final List<EventModel> events;

  EventsList(
    this.weekDay,
    this.day,
    this.month,
    this.events,
  );

  factory EventsList.fromMap(Map<dynamic, dynamic> data) {
    List<String> dateInfo = data["date"].split(" ");
    List<EventModel> events = [];
    for (Map<dynamic, dynamic> event in data["events"]) {
      events.add(EventModel.fromMap(event));
    }
    return EventsList(
      dateInfo[0],
      dateInfo[1],
      dateInfo[2],
      events,
    );
  }
}
