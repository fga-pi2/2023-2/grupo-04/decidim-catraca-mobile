import 'package:decidim_catraca_mobile/models/event_schedule.dart';
import 'package:flutter/material.dart';

class EventModel {
  final int id;
  final String name;
  final String date;
  final String location;
  final Color color;
  final List<EventSchedule> eventSchedule;

  EventModel(
    this.id,
    this.name,
    this.date,
    this.location,
    this.color,
    this.eventSchedule,
  );

  factory EventModel.fromMap(Map<dynamic, dynamic> data) {
    List<EventSchedule> listSchedule = [];
    String dateFromServer = data["date"];
    String newDateFormat = dateFromServer.substring(0, 10).replaceAll("-", "/");

    for (Map<dynamic, dynamic> schedule in data["schedule"]) {
      listSchedule.add(EventSchedule.fromMap(schedule));
    }
    return EventModel(
      data["id"],
      data["name"],
      newDateFormat,
      data["local"],
      hexToColor(data["color"]),
      listSchedule,
    );
  }

  static Color hexToColor(String hexString) {
    if (hexString.isEmpty) {
      return Colors.transparent; // ou qualquer cor padrão que você desejar
    }

    hexString = hexString.replaceAll("#", "");
    if (hexString.length == 6) {
      hexString =
          "FF$hexString"; // Adiciona a opacidade FF por padrão (totalmente opaco)
    }

    int colorValue = int.parse(hexString, radix: 16);
    return Color(colorValue);
  }
}
