class UserModel {
  final String fullName;
  final String id;
  final bool isAdmin;
  final String token;
  final String email;

  UserModel(
    this.fullName,
    this.id,
    this.isAdmin,
    this.token,
    this.email,
  );

  factory UserModel.fromMap(Map<String, dynamic> data, String email) {
    List<String> namesList = data["fullName"].split(" ");
    String name = "${namesList.first} ${namesList.last}";

    return UserModel(
      name,
      data["id"],
      data["isAdmin"],
      data["token"],
      email,
    );
  }
}
