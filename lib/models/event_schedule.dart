class EventSchedule {
  final int id;
  final String startTime;
  final String endTime;
  final String description;

  EventSchedule(
    this.id,
    this.startTime,
    this.endTime,
    this.description,
  );

  factory EventSchedule.fromMap(Map<dynamic, dynamic> data) {
    return EventSchedule(
      data["scheduleEventId"],
      data["startTime"],
      data["endTime"],
      data["description"],
    );
  }
}
