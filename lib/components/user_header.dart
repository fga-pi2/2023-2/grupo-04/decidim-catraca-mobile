import 'package:flutter/material.dart';

import '../shared/app_colors.dart';
import '../views/sign_in/sign_in.dart';

class UserHeader extends StatelessWidget {
  const UserHeader({
    super.key,
    this.showBackButton = false,
    this.showSignOutButton = false,
  });

  final bool showBackButton;
  final bool showSignOutButton;

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return SizedBox(
      height: size.height * 0.251,
      child: Stack(
        children: [
          Align(
            alignment: Alignment.topCenter,
            child: Container(
              height: size.height * 0.216,
              color: AppColors.primaryRed,
            ),
          ),
          SizedBox(
            height: size.height,
            width: size.width,
            child: Image.asset("assets/images/header/black-shape.png",
                fit: BoxFit.fitWidth),
          ),
          Center(
            child: SizedBox(
              width: size.width * 0.414,
              height: size.height * 0.168,
              child: Image.asset("assets/images/decidim-logo.png"),
            ),
          ),
          Visibility(
            visible: showBackButton,
            child: Positioned(
              top: size.height * 0.08,
              left: size.width * 0.05,
              child: IconButton(
                onPressed: () {
                  Navigator.pop(context);
                },
                icon: const Icon(
                  Icons.arrow_back_ios,
                  color: Colors.white,
                ),
              ),
            ),
          ),
          Visibility(
            visible: showSignOutButton,
            child: Positioned(
              top: size.height * 0.08,
              right: size.width * 0.05,
              child: IconButton(
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => const SignInPage(),
                    ),
                  );
                },
                icon: const Icon(
                  Icons.exit_to_app,
                  color: Colors.white,
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}
