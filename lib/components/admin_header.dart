// ignore_for_file: prefer_const_constructors

import 'package:decidim_catraca_mobile/views/sign_in/sign_in.dart';
import 'package:flutter/material.dart';

import '../shared/app_colors.dart';

class AdminHeader extends StatelessWidget {
  const AdminHeader(
      {super.key, this.showBackButton = false, this.showSignOutButton = false});

  final bool showBackButton;
  final bool showSignOutButton;

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return SizedBox(
      height: size.height * 0.251,
      child: Stack(
        children: [
          Align(
            alignment: Alignment.topCenter,
            child: Container(
              height: size.height * 0.216,
              color: AppColors.primaryRed,
            ),
          ),
          SizedBox(
            height: size.height,
            width: size.width,
            child: Image.asset(
              "assets/images/header/black-shape.png",
              fit: BoxFit.fill,
            ),
          ),
          Center(
            child: SizedBox(
              width: size.width * 0.414,
              height: size.height * 0.168,
              child: Image.asset("assets/images/decidim-logo.png"),
            ),
          ),
          Visibility(
            visible: showBackButton,
            child: Positioned(
              top: size.height * 0.08,
              left: size.width * 0.05,
              child: IconButton(
                onPressed: () {
                  Navigator.pop(context);
                },
                icon: Icon(
                  Icons.arrow_back_ios,
                  color: Colors.white,
                ),
              ),
            ),
          ),
          Visibility(
            visible: showSignOutButton,
            child: Positioned(
              top: size.height * 0.08,
              right: size.width * 0.05,
              child: IconButton(
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => SignInPage(),
                    ),
                  );
                },
                icon: Icon(
                  Icons.exit_to_app,
                  color: Colors.white,
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}
