import 'package:decidim_catraca_mobile/components/event_edit_card.dart';
import 'package:decidim_catraca_mobile/components/event_preview.dart';
import 'package:decidim_catraca_mobile/models/event.dart';
import 'package:flutter/material.dart';

import '../shared/shared.dart';

class DayEventsCard extends StatelessWidget {
  const DayEventsCard({
    super.key,
    required this.weekDay,
    required this.day,
    required this.month,
    required this.eventsList,
    required this.isEditEvent,
  });

  final String weekDay;
  final String day;
  final String month;
  final List<EventModel> eventsList;
  final bool isEditEvent;

  List<Widget> createEventsList(bool isEditEvent) {
    List<Widget> result = [];
    for (EventModel event in eventsList) {
      if (isEditEvent) {
        result.add(
          EventEditCard(event: event),
        );
      } else {
        result.add(EventPreview(event: event));
      }
    }
    return result;
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: size.width * 0.039),
      child: Container(
        width: size.width,
        padding: EdgeInsets.fromLTRB(
          size.width * 0.051,
          size.height * 0.022,
          size.width * 0.027,
          0,
        ),
        margin: EdgeInsets.only(top: size.height * 0.015),
        decoration: BoxDecoration(
          color: AppColors.cardBackground,
          borderRadius: BorderRadius.circular(20),
          boxShadow: [
            BoxShadow(
              color: Colors.black.withOpacity(.25),
              blurRadius: 4,
              spreadRadius: 0,
              offset: const Offset(0, 4),
            ),
          ],
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Align(
              alignment: Alignment.centerLeft,
              child: SizedBox(
                width: size.width * 0.15,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Text(
                      weekDay,
                      style: AppStyles.black12semiBold,
                    ),
                    Text(
                      day,
                      style: AppStyles.black32Bold,
                    ),
                    Text(
                      month,
                      style: AppStyles.black14Regular,
                    )
                  ],
                ),
              ),
            ),
            Column(
              children: createEventsList(isEditEvent),
            ),
          ],
        ),
      ),
    );
  }
}
