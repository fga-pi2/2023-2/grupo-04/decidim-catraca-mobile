import 'package:decidim_catraca_mobile/shared/app_colors.dart';
import 'package:decidim_catraca_mobile/shared/app_styles.dart';
import 'package:flutter/material.dart';

class ServerErrorDialog extends StatelessWidget {
  const ServerErrorDialog({super.key});

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Dialog(
      child: Container(
        height: size.height * 0.4,
        width: size.width * 0.7,
        padding: EdgeInsets.symmetric(horizontal: size.width * 0.03),
        child: Column(
          children: [
            Container(
              height: size.height * 0.15,
              width: size.width * 0.3,
              margin: EdgeInsets.only(
                top: size.height * 0.03,
              ),
              child: Image.asset(
                "assets/images/server-error.png",
              ),
            ),
            const Text(
              "Problema no servidor",
              style: AppStyles.black20TitleStyle,
            ),
            const Text(
              "Desculpe pelo transtorno! Estamos enfrentando dificuldades com o servidor",
              textAlign: TextAlign.center,
              style: AppStyles.black16Regular,
            ),
            Container(
              height: size.height * 0.04,
              width: size.width * 0.5,
              margin: EdgeInsets.only(top: size.height * 0.03),
              decoration: BoxDecoration(
                color: AppColors.primaryRed,
                borderRadius: BorderRadius.circular(10),
              ),
              child: ElevatedButton(
                onPressed: () {
                  Navigator.pop(context);
                },
                style: AppStyles.elevatedEventButton,
                child: const Text(
                  "Fechar!",
                  style: AppStyles.white12semiBold,
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
