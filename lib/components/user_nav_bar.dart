import 'package:decidim_catraca_mobile/views/user/home/user_home_controller.dart';
import 'package:flutter/material.dart';

import '../shared/shared.dart';

class UserNavBar extends StatefulWidget {
  const UserNavBar(
      {super.key,
      required this.alignSelected,
      required this.homeUserController});

  final AlignmentGeometry alignSelected;
  final UserHomeController homeUserController;

  @override
  State<UserNavBar> createState() => _UserNavBarState();
}

class _UserNavBarState extends State<UserNavBar> {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      width: size.width * 0.916,
      height: size.height * 0.071,
      margin: EdgeInsets.only(bottom: size.height * 0.052),
      decoration: BoxDecoration(
        color: Colors.black,
        borderRadius: BorderRadius.circular(40),
      ),
      child: Stack(children: [
        ValueListenableBuilder(
            valueListenable: widget.homeUserController.pageNotifier,
            builder: (context, value, child) => AnimatedAlign(
                  duration: const Duration(milliseconds: 300),
                  alignment: widget.homeUserController.getNavbarAlignment(),
                  child: Container(
                    height: size.height * 0.062,
                    width: size.width * 0.316,
                    margin:
                        EdgeInsets.symmetric(horizontal: size.width * 0.025),
                    decoration: BoxDecoration(
                      color: AppColors.primaryRed,
                      borderRadius: BorderRadius.circular(40),
                    ),
                  ),
                )),
        Center(
          child: SizedBox(
            width: size.width * 0.73,
            height: size.height,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                ElevatedButton(
                  onPressed: () {
                    widget.homeUserController.pageNotifier.value = "CALENDAR";
                  },
                  style: AppStyles.elevatedTextButton,
                  child: SizedBox(
                    height: size.height * 0.028,
                    width: size.width * 0.062,
                    child: Image.asset(
                      "assets/images/navbar/calendar.png",
                    ),
                  ),
                ),
                ElevatedButton(
                  onPressed: () {
                    widget.homeUserController.pageNotifier.value = "HOME";
                  },
                  style: AppStyles.elevatedTextButton,
                  child: SizedBox(
                    height: size.height * 0.028,
                    width: size.width * 0.062,
                    child: Image.asset(
                      "assets/images/navbar/home.png",
                    ),
                  ),
                ),
                ElevatedButton(
                  onPressed: () {
                    widget.homeUserController.pageNotifier.value = "QRCODE";
                  },
                  style: AppStyles.elevatedTextButton,
                  child: SizedBox(
                    height: size.height * 0.028,
                    width: size.width * 0.062,
                    child: Image.asset(
                      "assets/images/navbar/qr-code.png",
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ]),
    );
  }
}
