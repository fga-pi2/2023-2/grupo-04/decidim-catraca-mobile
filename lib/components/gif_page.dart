import 'package:flutter/material.dart';
import 'package:flutter_gif/flutter_gif.dart';

class GifState extends StatefulWidget {
  const GifState({super.key, required this.error, required this.function});

  final bool error;
  final Function function;

  @override
  State<GifState> createState() => _GifStateState();
}

class _GifStateState extends State<GifState> with TickerProviderStateMixin {
  late FlutterGifController gifController;

  @override
  void initState() {
    gifController = FlutterGifController(vsync: this);
    super.initState();
  }

  @override
  void dispose() {
    gifController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    gifController.value = 0;

    gifController
        .animateTo(widget.error ? 36 : 46,
            duration: const Duration(milliseconds: 2500))
        .whenComplete(() {
      widget.function();
    });
    return Container(
      height: size.height,
      width: size.width,
      color: const Color(0xff252222).withOpacity(.8),
      child: Center(
        child: SizedBox(
          height: size.height * 0.184,
          child: GifImage(
            controller: gifController,
            image: widget.error
                ? const AssetImage("assets/gifs/error.gif")
                : const AssetImage("assets/gifs/done.gif"),
          ),
        ),
      ),
    );
  }
}
