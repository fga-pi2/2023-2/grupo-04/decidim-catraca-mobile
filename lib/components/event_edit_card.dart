import 'package:decidim_catraca_mobile/models/event.dart';
import 'package:decidim_catraca_mobile/views/admin/events/components/event_options.dart';
import 'package:flutter/material.dart';

import '../shared/shared.dart';

class EventEditCard extends StatelessWidget {
  const EventEditCard({super.key, required this.event});

  final EventModel event;

  List<Widget> createScheduleOnCard() {
    List<Widget> result = [];

    for (var element in event.eventSchedule) {
      result.add(
        Row(
          children: [
            Text(
                "${element.startTime.toLowerCase().replaceAll(" ", "")}-${element.endTime.toLowerCase().replaceAll(" ", "")} ",
                style: AppStyles.white12Regular),
            Text(
              element.description,
              style: AppStyles.white12semiBold,
            )
          ],
        ),
      );
    }

    if (result.isEmpty) {
      result.add(const Row(
        children: [
          Text("--:-- --:-- ", style: AppStyles.white12Regular),
          Text(
            "--------",
            style: AppStyles.white12semiBold,
          )
        ],
      ));
    }

    return result;
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      width: size.width * 0.637,
      margin: EdgeInsets.only(bottom: size.height * 0.0225),
      decoration: BoxDecoration(
        color: event.color,
        borderRadius: BorderRadius.circular(10),
        boxShadow: [
          BoxShadow(
            color: Colors.black.withOpacity(.25),
            blurRadius: 4,
            spreadRadius: 0,
            offset: const Offset(0, 4),
          )
        ],
      ),
      child: ElevatedButton(
        onPressed: () {
          showModalBottomSheet(
            context: context,
            builder: (context) => EventSheetOptions(
              event: event,
            ),
            backgroundColor: Colors.black.withOpacity(.2),
            isDismissible: true,
          );
        },
        style: ButtonStyle(
          padding: MaterialStatePropertyAll(
            EdgeInsets.fromLTRB(
              size.width * 0.017,
              size.height * 0.011,
              size.width * 0.032,
              size.height * 0.016,
            ),
          ),
          shadowColor: const MaterialStatePropertyAll(
            Colors.transparent,
          ),
          backgroundColor: const MaterialStatePropertyAll(
            Colors.transparent,
          ),
          surfaceTintColor: const MaterialStatePropertyAll(
            Colors.transparent,
          ),
          overlayColor: MaterialStatePropertyAll(
            Colors.white.withOpacity(.3),
          ),
          shape: const MaterialStatePropertyAll(LinearBorder()),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.end,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                SizedBox(
                  width: size.width * 0.05 + 5,
                ),
                Expanded(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(
                        width: size.width * 0.472,
                        child: Text(
                          event.name,
                          style: AppStyles.white13TitleStyle,
                        ),
                      ),
                      const Icon(
                        Icons.edit,
                        color: Colors.white,
                        size: 16,
                      )
                    ],
                  ),
                )
              ],
            ),
            Container(
              margin: EdgeInsets.only(top: size.height * 0.01),
              child: Row(
                children: [
                  SizedBox(
                    width: size.width * 0.05,
                    child: const Center(
                      child: Icon(
                        Icons.access_time_filled,
                        color: Colors.white,
                        size: 14,
                      ),
                    ),
                  ),
                  const SizedBox(
                    width: 5,
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: createScheduleOnCard(),
                  )
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: size.height * 0.01),
              child: Row(
                children: [
                  SizedBox(
                    width: size.width * 0.05,
                    child: const Center(
                      child: Icon(
                        Icons.location_on,
                        color: Colors.white,
                        size: 14,
                      ),
                    ),
                  ),
                  const SizedBox(
                    width: 5,
                  ),
                  SizedBox(
                    width: size.width * 0.57 - size.width * 0.05 - 5,
                    child: Text(
                      event.location,
                      style: AppStyles.white12Regular,
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
