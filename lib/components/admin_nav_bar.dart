import 'package:decidim_catraca_mobile/views/admin/home/controller/admin_home_controller.dart';
import 'package:flutter/material.dart';

import '../shared/shared.dart';

class AdminNavBar extends StatefulWidget {
  const AdminNavBar(
      {super.key, required this.alignSelected, required this.controller});

  final AlignmentGeometry alignSelected;
  final AdminHomeController controller;

  @override
  State<AdminNavBar> createState() => _AdminNavBarState();
}

class _AdminNavBarState extends State<AdminNavBar> {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      width: size.width * 0.916,
      height: size.height * 0.071,
      margin: EdgeInsets.only(bottom: size.height * 0.052),
      decoration: BoxDecoration(
        color: Colors.black,
        borderRadius: BorderRadius.circular(40),
      ),
      child: Stack(children: [
        ValueListenableBuilder(
          valueListenable: widget.controller.page,
          builder: (context, value, child) => AnimatedAlign(
            duration: const Duration(milliseconds: 300),
            alignment: widget.controller.getNavbarAlignment(),
            child: Container(
              height: size.height * 0.062,
              width: size.width * 0.316,
              margin: EdgeInsets.symmetric(horizontal: size.width * 0.025),
              decoration: BoxDecoration(
                color: AppColors.primaryRed,
                borderRadius: BorderRadius.circular(40),
              ),
            ),
          ),
        ),
        Center(
          child: SizedBox(
            width: size.width * 0.73,
            height: size.height,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                ElevatedButton(
                  onPressed: () {
                    widget.controller.page.value = PageSelected.EVENTS;
                  },
                  style: AppStyles.elevatedTextButton,
                  child: SizedBox(
                    height: size.height * 0.028,
                    width: size.width * 0.062,
                    child: Image.asset(
                      "assets/images/navbar/calendar.png",
                    ),
                  ),
                ),
                ElevatedButton(
                  onPressed: () {
                    widget.controller.page.value = PageSelected.HOME;
                  },
                  style: AppStyles.elevatedTextButton,
                  child: SizedBox(
                    height: size.height * 0.028,
                    width: size.width * 0.062,
                    child: Image.asset(
                      "assets/images/navbar/home.png",
                    ),
                  ),
                ),
                ElevatedButton(
                  onPressed: () {
                    widget.controller.page.value = PageSelected.QR;
                  },
                  style: AppStyles.elevatedTextButton,
                  child: SizedBox(
                    height: size.height * 0.028,
                    width: size.width * 0.062,
                    child: Image.asset(
                      "assets/images/navbar/qr-code.png",
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ]),
    );
  }
}
