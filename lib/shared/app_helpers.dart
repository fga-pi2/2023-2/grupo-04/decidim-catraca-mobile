import 'package:intl/intl.dart';

class AppHelpers {
  static String transformServerDataIntoExtMonth(String? date) {
    if (date == null) return "";
    DateTime dateTime = DateTime.parse(date);

    String formattedDate = DateFormat('MMM. dd').format(dateTime);
    return formattedDate;
  }
}
