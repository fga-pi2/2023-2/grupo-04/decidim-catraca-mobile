import 'package:flutter/material.dart';

class AppColors {
  static const primaryRed = Color(0XFFE31837);
  static const black = Colors.black;
  static const cardBackground = Color(0XFFF7F7F7);
  static const greyText = Color(0XFF8A8888);
  static const greyProgressBar = Color(0XFFD9D9D9);
}
