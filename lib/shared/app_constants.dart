// ignore_for_file: constant_identifier_names

import 'package:decidim_catraca_mobile/models/event.dart';
import 'package:decidim_catraca_mobile/models/event_schedule.dart';
import 'package:decidim_catraca_mobile/models/user_model.dart';

import 'app_colors.dart';

class AppConstants {
  static const BASE_URL = "http://54.207.129.126:3333";

  static EventModel event1 = EventModel(1, "Eleição presidente", "12/12/2023",
      "Ulysses Guimarães", AppColors.primaryRed, [schedule1, schedule2]);

  static EventModel event2 = EventModel(2, "Premiação destaques do ano",
      "17/12/2023", "Nilson Nelson", AppColors.black, [schedule3]);

  static EventSchedule schedule1 = EventSchedule(1, "09:30am", "11:30am", "");
  static EventSchedule schedule2 = EventSchedule(2, "11:30am", "02:30pm", "");

  static EventSchedule schedule3 = EventSchedule(4, "09:30am", "11:30am", "");

  static late UserModel actualUser;
}
