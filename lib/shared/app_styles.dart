import 'package:decidim_catraca_mobile/shared/app_colors.dart';
import 'package:flutter/material.dart';

class AppStyles {
  // Text
  static const whiteTitleStyle = TextStyle(
    color: Colors.white,
    fontSize: 32,
    fontWeight: FontWeight.bold,
  );

  static const pageTitle24 = TextStyle(
    color: Colors.black,
    fontSize: 24,
    fontWeight: FontWeight.bold,
  );

  static const pageTitle18 = TextStyle(
    color: Colors.black,
    fontSize: 18,
    fontWeight: FontWeight.bold,
  );

  static const inputLabelStyle = TextStyle(
    color: Colors.black,
    fontSize: 14,
    fontWeight: FontWeight.bold,
  );

  static const inputRedLabelStyle = TextStyle(
    color: AppColors.primaryRed,
    fontSize: 14,
    fontWeight: FontWeight.bold,
  );

  static const black10TitleStyle = TextStyle(
    fontSize: 10,
    color: Colors.black,
    fontWeight: FontWeight.bold,
    decoration: TextDecoration.underline,
  );

  static const black13TitleStyle = TextStyle(
    fontSize: 13,
    color: Colors.black,
    fontWeight: FontWeight.bold,
  );

  static const black20TitleStyle = TextStyle(
    fontSize: 20,
    color: Colors.black,
    fontWeight: FontWeight.bold,
  );

  static const white13TitleStyle = TextStyle(
    fontSize: 13,
    color: Colors.white,
    fontWeight: FontWeight.bold,
  );

  static const white14TitleStyle = TextStyle(
    fontSize: 14,
    color: Colors.white,
    fontWeight: FontWeight.bold,
  );

  static const red13TitleStyle = TextStyle(
    fontSize: 13,
    color: AppColors.primaryRed,
    fontWeight: FontWeight.bold,
  );

  static const red10TitleStyle = TextStyle(
    fontSize: 10,
    color: AppColors.primaryRed,
    fontWeight: FontWeight.bold,
  );

  static const red40TitleStyle = TextStyle(
    fontSize: 40,
    color: AppColors.primaryRed,
    fontWeight: FontWeight.bold,
  );

  static const red20Regular = TextStyle(
    fontSize: 20,
    color: AppColors.primaryRed,
  );

  static const white20ButtonTitle = TextStyle(
    fontSize: 20,
    color: Colors.white,
    fontWeight: FontWeight.bold,
  );

  static const white16ButtonTitle = TextStyle(
    fontSize: 16,
    color: Colors.white,
    fontWeight: FontWeight.bold,
  );

  static const black16Regular = TextStyle(
    fontSize: 16,
    color: Colors.black,
  );

  static const grey20Regular = TextStyle(
    fontSize: 20,
    color: AppColors.greyText,
  );

  static const black32Bold = TextStyle(
    fontSize: 32,
    color: Colors.black,
    fontWeight: FontWeight.bold,
  );

  static const black32Regular = TextStyle(
    fontSize: 32,
    color: Colors.black,
  );

  static const white32Bold = TextStyle(
    fontSize: 32,
    color: Colors.white,
    fontWeight: FontWeight.bold,
  );

  static const white40Bold = TextStyle(
    fontSize: 40,
    color: Colors.white,
    fontWeight: FontWeight.bold,
  );

  static const black12semiBold = TextStyle(
    color: Colors.black,
    fontFamily: "Inter-SemiBold",
    fontSize: 12,
    fontWeight: FontWeight.w600,
  );

  static const white12semiBold = TextStyle(
    color: Colors.white,
    fontFamily: "Inter-SemiBold",
    fontSize: 12,
    fontWeight: FontWeight.w600,
  );

  static const black14Regular = TextStyle(
    color: Colors.black,
    fontSize: 14,
  );

  static const white12Regular = TextStyle(
    color: Colors.white,
    fontSize: 12,
  );

  static const white20Regular = TextStyle(
    color: Colors.white,
    fontSize: 20,
  );

  // Button
  static final elevatedTextButton = ButtonStyle(
    padding:
        const MaterialStatePropertyAll(EdgeInsets.symmetric(horizontal: 10)),
    shadowColor: const MaterialStatePropertyAll(Colors.transparent),
    backgroundColor: const MaterialStatePropertyAll(Colors.transparent),
    surfaceTintColor: const MaterialStatePropertyAll(Colors.transparent),
    overlayColor:
        MaterialStatePropertyAll(AppColors.primaryRed.withOpacity(.1)),
  );

  static final elevatedNavBarButton = ButtonStyle(
    padding: const MaterialStatePropertyAll(EdgeInsets.zero),
    shadowColor: const MaterialStatePropertyAll(Colors.transparent),
    backgroundColor: const MaterialStatePropertyAll(Colors.transparent),
    surfaceTintColor: const MaterialStatePropertyAll(Colors.transparent),
    overlayColor:
        MaterialStatePropertyAll(AppColors.primaryRed.withOpacity(.1)),
    shape: const MaterialStatePropertyAll(LinearBorder()),
    maximumSize: const MaterialStatePropertyAll(
      Size(27, 27),
    ),
    minimumSize: const MaterialStatePropertyAll(
      Size(27, 27),
    ),
  );

  static final elevatedRedButtonStyle = ButtonStyle(
    padding: const MaterialStatePropertyAll(EdgeInsets.zero),
    shadowColor: const MaterialStatePropertyAll(Colors.transparent),
    backgroundColor: const MaterialStatePropertyAll(Colors.transparent),
    surfaceTintColor: const MaterialStatePropertyAll(Colors.transparent),
    overlayColor: MaterialStatePropertyAll(Colors.white.withOpacity(.3)),
  );

  static final elevatedEventButton = ButtonStyle(
    padding: const MaterialStatePropertyAll(EdgeInsets.zero),
    shadowColor: const MaterialStatePropertyAll(Colors.transparent),
    backgroundColor: const MaterialStatePropertyAll(Colors.transparent),
    surfaceTintColor: const MaterialStatePropertyAll(Colors.transparent),
    overlayColor: MaterialStatePropertyAll(Colors.white.withOpacity(.3)),
  );

  static final redButton = ButtonStyle(
    padding: const MaterialStatePropertyAll(EdgeInsets.zero),
    shadowColor: const MaterialStatePropertyAll(Colors.black),
    backgroundColor: const MaterialStatePropertyAll(AppColors.primaryRed),
    surfaceTintColor: const MaterialStatePropertyAll(Colors.transparent),
    overlayColor: MaterialStatePropertyAll(Colors.white.withOpacity(.3)),
  );
}
