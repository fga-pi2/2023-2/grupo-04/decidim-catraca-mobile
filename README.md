# decidim_catraca_mobile


| Integrantes                   |
|------------------------|
| Abner Filipe           |
| Allecsander Lélis      |
| Ana Aparecida          |
| Bianca Sofia     |
| Daniel Primo           |
| Júlio César            |
| Lorrany Souza          |
| Lorrayne Alves        |
| Lucas Pimentel         |
| Nicolas                |
| Marina Costa           |
| Pedro Campos           |
| Rafael Leão            |
| Ramires                |
| Thiago Galletti        |
| Thiago França Vale Oliveira |
| Vitor Magalhães        |

## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://docs.flutter.dev/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://docs.flutter.dev/cookbook)

For help getting started with Flutter development, view the
[online documentation](https://docs.flutter.dev/), which offers tutorials,
samples, guidance on mobile development, and a full API reference.
